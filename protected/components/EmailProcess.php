<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailProcess
 *
 * @author FamLopBar
 */
class EmailProcess {
    //put your code here
    
    public function SendMail()
    {   
        $message            = new YiiMailMessage;
           //this points to the file test.php inside the view path
        $message->view = "test";
        $sid                 = 1;
        //$criteria            = new CDbCriteria();
        //$criteria->condition = "studentID=".$sid."";  
        $id = Yii::app()->user->GetState('id');
        $User          = User::model()->findByPk($id);        
        $params              = array('myMail'=>$User);
        $message->subject    = 'My TestSubject';
        $message->setBody($params, 'text/html');                
        $message->addTo('stevelopezc@yahoo.com');
        $message->from = Yii::app()->params['adminEmail'];   
        Yii::app()->mail->send($message);       
    }
    
    public function SendMailNewStudent($id)
    {   
        return;
        
        $student          = Student::model()->findByPk($id);   
        if ($student)
        {
            if ($student->email && trim($student->email)!='')
            {
                $message            = new YiiMailMessage;
                $message->view = "Newstudent";

                $params              = array('myMail'=>$student);
                $message->subject    = 'Alta Estudiante';
                $message->setBody($params, 'text/html');                
                $message->addTo($student->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailRequestPassword($id)
    {   
        $user          = User::model()->findByPk($id);   
        if ($user)
        {
            if ($user->email && trim($user->email)!='')
            {
                $message            = new YiiMailMessage;
                $message->view = "RequestPassword";

                $params              = array('myMail'=>$user);
                $message->subject    = 'Reinicio de clave de Acceso';
                $message->setBody($params, 'text/html');                
                $message->addTo($user->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailNewUser($id)
    {   
        $user          = User::model()->findByPk($id);   
        if ($user)
        {
            if ($user->email && trim($user->email)!='')
            {
                $message            = new YiiMailMessage;
                $message->view = "NewUser";

                $params              = array('myMail'=>$user);
                $message->subject    = 'Datos de acceso';
                $message->setBody($params, 'text/html');                
                $message->addTo($user->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailRequestChangeStatus($id)
    {   
        $request          = Request::model()->findByPk($id);   
        if ($request)
        {
            if ($request->student->email && trim($request->student->email)!='')
            {
                Yii::app()->user->SetState('TipoGestion','Solicitud Autorizacion de Tema');
                
                $message            = new YiiMailMessage;
                $message->view = "ChangeStatusNotificacion";

                $params              = array('myMail'=>$request);
                $message->subject    = 'Notificación cambio de Estado';
                $message->setBody($params, 'text/html');                
                $message->addTo($request->student->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailPreProjectChangeStatus($id)
    {   
        $request          = Preproject::model()->findByPk($id);   
        if ($request)
        {
            if ($request->student->email && trim($request->student->email)!='')
            {
                Yii::app()->user->SetState('TipoGestion','Anteproyecto de Investigación');
                
                $message            = new YiiMailMessage;
                $message->view = "ChangeStatusNotificacion";

                $params              = array('myMail'=>$request);
                $message->subject    = 'Notificación cambio de Estado';
                $message->setBody($params, 'text/html');                
                $message->addTo($request->student->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailProjectChangeStatus($id)
    {   
        $request          = Project::model()->findByPk($id);   
        if ($request)
        {
            if ($request->student->email && trim($request->student->email)!='')
            {
                Yii::app()->user->SetState('TipoGestion','Proyecto de Investigación');
                
                $message            = new YiiMailMessage;
                $message->view = "ChangeStatusNotificacion";

                $params              = array('myMail'=>$request);
                $message->subject    = 'Notificación cambio de Estado';
                $message->setBody($params, 'text/html');                
                $message->addTo($request->student->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    
    public function SendMailAsignPreProjectReview($id)
    {   
        $preprojectreviewer          = Preprojectreviewer::model()->findByPk($id);   
        if ($preprojectreviewer)
        {
            if ($preprojectreviewer->teacher->email && trim($preprojectreviewer->teacher->email)!='')
            {
                Yii::app()->user->SetState('TipoGestion','Anteproyecto de Investigación');
                
                $message            = new YiiMailMessage;
                $message->view = "ReviewNotifier";

                $params              = array('myMail'=>$preprojectreviewer);
                $message->subject    = 'Notificación asignación Revision';
                $message->setBody($params, 'text/html');                
                $message->addTo($preprojectreviewer->teacher->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
    
    public function SendMailAsignProjectReview($id)
    {   
        $projectreview          = Projectreview::model()->findByPk($id);   
        if ($projectreview)
        {
            if ($projectreview->teacher->email && trim($projectreview->teacher->email)!='')
            {
                Yii::app()->user->SetState('TipoGestion','Proyecto de Investigación');
                
                $message            = new YiiMailMessage;
                $message->view = "ReviewNotifier";

                $params              = array('myMail'=>$projectreview);
                $message->subject    = 'Notificación asignación Revision';
                $message->setBody($params, 'text/html');                
                $message->addTo($projectreview->teacher->email);
                $message->from = array(Yii::app()->params['adminEmail'] => Yii::app()->name);      
                Yii::app()->mail->send($message);  
            }
        }
    }
}
