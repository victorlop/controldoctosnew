<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DocumentsValidation
 *
 * @author XPMUser
 */
class DocumentsFlowOperation {
    public static function CanModifyRequest($request)
    {
        $valpar=(int)$request->flowdoctoid;
        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
        $criteria->condition = 'flowid=:flwid';
        $criteria->params=array(':flwid'=>$valpar);

        $flowdocto= FlowdoctoDetail::model()->find($criteria);
        if ($flowdocto)
            $statusid = (int)$flowdocto->currentstatusid;
        
        $dis = $request->statusid != $statusid? TRUE : FALSE;
        
        return $dis;
    }
    
    public static function CanModifyRequestMaxTopic($request)
    {
        $cuantos = Requestdetail::model()->count('requestid=:requestid',array(':requestid'=>$request->id));
        $numtp = Appparameter::model()->find('code =:req',array(':req'=>'NUMTP'));            
        
        return $cuantos<(int)$numtp->parametervalue? TRUE : FALSE;
    }
    
    public static function RequestMaxTopic()
    {
        $numtp = Appparameter::model()->find('code =:req',array(':req'=>'NUMTP'));            
        
        return (int)$numtp->parametervalue;
    }
    public static function RequestDefaultFlow()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'REQ'));

        if ($flow)
            return $flow;
        
        return null;   
    }
    
    public static function RequestInitialFlowStatus()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'REQ'));

        if ($flow)
        {
            $valpar=(int)$flow->parametervalue;
            $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
            $criteria->condition = 'flowid=:flwid';
            $criteria->params=array(':flwid'=>$valpar);

            $flowdocto= FlowdoctoDetail::model()->find($criteria);
            if ($flowdocto)
                return (int)$flowdocto->currentstatusid;
        }
        return null;
    }
    
    public static function NextFlowStatus($currentStatus, $flowdocto){
        $criteria = new CDbCriteria (array('order'=> 'sort ASC'));
        $criteria->condition = 'flowid=:flwid and currentstatusid=:currentstatus';
        $criteria->params = array(':flwid'=>$flowdocto,':currentstatus'=>$currentStatus);
        $detail = FlowdoctoDetail::model()->findAll($criteria);
        $status = array();
        $st = Status::model()->findByPk($currentStatus);
        if ($st) $status[$st->id] = $st;
        foreach ($detail as $item) {
            $st = Status::model()->findByPk($item->newstatusid);
            if ($st) $status[$st->id] = $st;
        }
        
        return $status;
    }
    
    public static function RequestGetEndStatus ($requestid)
    {
        $request = Request::model()->findByPk((int)$requestid);
        if ($request)
            return !$request->status->endstatus;
        
        return true;
        
    }
    /*ANTEPROYECTO*/
    public static function PreProjectDefaultFlow()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'ANT'));

        if ($flow)
            return $flow;
        
        return null;   
    }
    
    public static function PreProjectInitialFlowStatus()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'ANT'));

        if ($flow)
        {
            $valpar=(int)$flow->parametervalue;
            $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
            $criteria->condition = 'flowid=:flwid';
            $criteria->params=array(':flwid'=>$valpar);

            $flowdocto= FlowdoctoDetail::model()->find($criteria);
            if ($flowdocto)
                return (int)$flowdocto->currentstatusid;
        }
        
        return null;
    
    }
    
    public static function CanModifyPreProject($PrepProject)
    {
        $valpar=(int)$PrepProject->flowdoctoid;
        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
        $criteria->condition = 'flowid=:flwid';
        $criteria->params=array(':flwid'=>$valpar);

        $flowdocto= FlowdoctoDetail::model()->find($criteria);
        if ($flowdocto)
            $statusid = (int)$flowdocto->currentstatusid;
        
        $dis = $PrepProject->statusid != $statusid? TRUE : FALSE;
        
        return $dis;
    }
    
    public static function PreProjectGetEndStatus ($preprojectid)
    {
        $preproject = Preproject::model()->findByPk((int)$preprojectid);

        if ($preproject)
            return !$preproject->status->endstatus;
        
        return true;
        
    }
    
    /*flow*/
    public static function InitialFlowStatus($flowid)
    {
        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
        $criteria->condition = 'flowid=:flwid';
        $criteria->params=array(':flwid'=>$flowid);

        $flowdocto= FlowdoctoDetail::model()->find($criteria);
        if ($flowdocto)
            return (int)$flowdocto->currentstatusid;
       
        return null;
    
    }
    
    /*PROYECTO*/
    public static function ProjectDefaultFlow()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'PRJ'));

        if ($flow)
            return $flow;
        
        return null;   
    }
    
    public static function ProjectInitialFlowStatus()
    {
        $flow = Appparameter::model()->find('code =:req',array(':req'=>'PRJ'));

        if ($flow)
        {
            $valpar=(int)$flow->parametervalue;
            $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
            $criteria->condition = 'flowid=:flwid';
            $criteria->params=array(':flwid'=>$valpar);

            $flowdocto= FlowdoctoDetail::model()->find($criteria);
            if ($flowdocto)
                return (int)$flowdocto->currentstatusid;
        }
        
        return null;
    
    }
    
    public static function CanModifyProject($Project)
    {
        $valpar=(int)$Project->flowdoctoid;
        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
        $criteria->condition = 'flowid=:flwid';
        $criteria->params=array(':flwid'=>$valpar);

        $flowdocto= FlowdoctoDetail::model()->find($criteria);
        if ($flowdocto)
            $statusid = (int)$flowdocto->currentstatusid;
        
        $dis = $Project->statusId != $statusid? TRUE : FALSE;
        
        return $dis;
    }
    
    public static function CanModifyProjectReportTask($Project)
    {
        $dis = $Project->completed? TRUE : FALSE;
        
        return $dis;
    }
    public static function ProjectGetEndStatus ($projectid)
    {
        $project = Project::model()->findByPk((int)$projectid);

        if ($project)
            return !$project->status->endstatus;
        
        return true;
    }
    
    /*REVISORES*/
    public static function CanModifyReviewers($preproject)
    {
        $valpar=(int)$preproject->flowdoctoid;
        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
        $criteria->condition = 'flowid=:flwid';
        $criteria->params=array(':flwid'=>$valpar);

        $flowdocto= FlowdoctoDetail::model()->find($criteria);
        if ($flowdocto)
            $statusid = (int)$flowdocto->currentstatusid;
        
        $dis = $request->statusid != $statusid? TRUE : FALSE;
        
        return $dis;
    }
    
    public static function CanModifyPreProjectMaxReviewers($preproject)
    {
        $minRev =(int)$preproject->minreviewerid;
        if((int)$preproject->firstreview==0)
            $minRev = $minRev + 1;
                
        $cuantos = Preprojectreviewer::model()->count('preprojectid=:preprojectid and statusid=:statusid and id>=:minrev',array(':preprojectid'=>$preproject->id,':statusid'=>$preproject->statusid,':minrev'=>$minRev));
        $numtp = Status::model()->findByPk((int)$preproject->statusid);
        
        if ($numtp->amountreviews == 0) return TRUE;
        
        return $cuantos<(int)$numtp->amountreviews? TRUE : FALSE;
    }
    
    public static function MaxReviewers($statusid)
    {
        $numtp = Status::model()->findByPk((int)$statusid);
        
        return (int)$numtp->amountreviews;
    }
    
    public static function CanModifyProjectMaxReviewers($project)
    {
        $minRev =(int)$project->minreviewerid;
        if((int)$project->firstreview==0)
            $minRev = $minRev + 1;
                
        $cuantos = Projectreview::model()->count('projectid=:projectid and statusId=:statusid and id>=:minrev',array(':projectid'=>$project->id,':statusid'=>$project->statusId,':minrev'=>$minRev));
        $numtp = Status::model()->findByPk((int)$project->statusId);
        
        if ($numtp->amountreviews == 0) return TRUE;
        
        return $cuantos<(int)$numtp->amountreviews? TRUE : FALSE;
    }
 }
