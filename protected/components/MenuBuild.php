<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuBuild
 *
 * @author FamLopBar
 */
class MenuBuild {
    //put your code here
    private static $menuTree = array();
 
   public static function getMenuTree($rolid) {
       
       $chump=array();

        if (empty(self::$menuTree)) {
            $rows = Module::model()->findAll();
            self::$menuTree[] = array('label'=>'Inicio', 'url'=>array('/site/index'));
	    //self::$menuTree[] = array('label'=>'Contact', 'url'=>array('/site/contact'));
	    self::$menuTree[] = array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest);
            foreach ($rows as $item) {
                
                $chump=self::getMenuItems($item,$rolid);
                if ($chump) {
                    self::$menuTree[] = 
                            array(  'label' => $item->title, 
                                    'url' => Yii::app()->createUrl($item->controller),
                                    //'submenuOptions'=>array(),
                                    'items' => $chump, 
                            );
                }
            }
            //self::$menuTree[] = array('label'=>'Acerca de', 'url'=>array('/site/page', 'view'=>'about'));
            self::$menuTree[] = array('label'=>'Cambio de Clave', 'url'=>array('/user/changepass'), 'visible'=>!Yii::app()->user->isGuest);
            self::$menuTree[] = array('label'=>'Salir', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest);
        }
        return self::$menuTree;
    }
 
    private static function getMenuItems($modelRow,$rolid) { 
        if (!$modelRow) {
            return;
        }
        $arr = array();
        
        $stOption = Systemoption::model()->with('rolsystemoptions')->findAll('moduleid= :moduleid and rolsystemoptions.rolid = :roleid',array(':moduleid'=>$modelRow->id,':roleid'=>$rolid));
        foreach ($stOption as $item)
        {
            //$chump=array('label' => ($item->title), 'url' => $item->path);
            $arr[]=array(
                    'label' => $item->title, 
                    'url' => Yii::app()->createUrl($item->path),
                    );
        }        
         return $arr;       
        

    }
}
//http://www.yiiframework.com/wiki/523/generate-a-multi-lever-array-sub-category-for-a-menu/