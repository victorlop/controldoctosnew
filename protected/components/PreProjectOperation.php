<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PreProjectOperation
 *
 * @author FamLopBar
 */
class PreProjectOperation {
    //put your code here
    public static function ShowReviewers($status)
    {
        $sta = Status::model()->findByPk((int)$status);
        if ($sta)
            return $sta->amountreviews > 0;
        
        return FALSE;
    }
}
