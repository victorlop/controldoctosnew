<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashBoard
 *
 * @author XPMUser
 */
class DashBoard {
    //put your code here
    public static function RequestLastMonth()
    {
        //<EmailProcess::SendMail();
        
        $criteria= new CDbCriteria();
        //$criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $criteria->compare('active =1 and entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos= Request::model()->count($criteria);

        return $cuantos;
    }
    
    public static function ListRequestLastMonth()
    {
        $criteria= new CDbCriteria(array('order'=>'entrydate DESC'));
        $criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos = Request::model()->findAll($criteria);
        
        return new CActiveDataProvider('Request', array(
                'criteria' => $criteria,
                'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
        ));
    }
    
    public static function PreProjectLastMonth()
    {
        //<EmailProcess::SendMail();
        
        $criteria= new CDbCriteria();
        //$criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $criteria->compare('active =1 and dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos= Preproject::model()->count($criteria);

        return $cuantos;
    }
    
    public static function ListPreProjectLastMonth()
    {
        $criteria= new CDbCriteria(array('order'=>'dateentry DESC'));
        $criteria->compare('dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos = Preproject::model()->findAll($criteria);
        
        return new CActiveDataProvider('Preproject', array(
                'criteria' => $criteria,
                'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
        ));
    }
    
    public static function ProjectLastMonth()
    {
        //<EmailProcess::SendMail();
        
        $criteria= new CDbCriteria();
        //$criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $criteria->compare('active =1 and dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos= Project::model()->count($criteria);

        return $cuantos;
    }
    
    public static function ListProjectLastMonth()
    {
        $criteria= new CDbCriteria(array('order'=>'dateentry DESC'));
        $criteria->compare('dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $cuantos = Project::model()->findAll($criteria);
        
        return new CActiveDataProvider('Project', array(
                'criteria' => $criteria,
                'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
        ));
    }
    
    public static function ProjectLastUpdated()
    {
        //<EmailProcess::SendMail();
        $chpdue = AppParameterOperation::EnableValue('CHPDUE');
        
        $criteria= new CDbCriteria();
        //$criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $criteria->compare('dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -'.$chpdue.' day')),TRUE);
        $criteria->select ='projectid';
        $criteria->distinct=TRUE;
        
        $prjtask = Projectreporttask::model()->findAll($criteria);
        
        $prjUpd = array();
        foreach ($prjtask as $prj) {
            $prjUpd[] = $prj->projectid;
        }
        
        $criteria = new CDbCriteria;//(array('condition'=>'completed = 0 and active=1'));
        $criteria->addCondition ('completed=0','AND');
        $criteria->addCondition('active=1','AND');
        
        $criteria->addNotInCondition('id',$prjUpd);
        $cuantos= Project::model()->count($criteria);       

        return $cuantos;
    }
    
    public static function ListProjectLastUpdated()
    {
        $chpdue = AppParameterOperation::EnableValue('CHPDUE');
        
        $criteria= new CDbCriteria();
        //$criteria->compare('entrydate','>='. date('Y-m-d 00:00:00', strtotime('now -30 day')),TRUE);
        $criteria->compare('dateentry','>='. date('Y-m-d 00:00:00', strtotime('now -'.$chpdue.' day')),TRUE);
        $criteria->select ='projectid';
        $criteria->distinct=TRUE;
        
        $prjtask = Projectreporttask::model()->findAll($criteria);
        
        $prjUpd = array();
        foreach ($prjtask as $prj) {
            $prjUpd[] = $prj->projectid;
        }
        
        $criteria = new CDbCriteria;//(array('condition'=>'completed = 0 and active=1'));
        $criteria->addCondition ('completed=0','AND');
        $criteria->addCondition('active=1','AND');
        
        $criteria->addNotInCondition('id',$prjUpd);
        
        return new CActiveDataProvider('Project', array(
                'criteria' => $criteria,
                'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
        ));
    }
}
//http://www.yiiframework.com/extension/yiimailer/