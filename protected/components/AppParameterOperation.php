<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppParameterOperation
 *
 * @author XPMUser
 */
class AppParameterOperation {
    //put your code here
    public static function EnableValue($parametercode)
    {
        $apppar= Appparameter::model()->find('code=:code',array(':code'=>$parametercode));
        if ($apppar)
            return (int)$apppar->parametervalue;

        return 0;
    }
}
