<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ChangePass extends CFormModel
{
	public $password;
	public $passwordnew;
	public $passwordconf;
        
        const WEAK = 0;
        const STRONG = 1;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
                        array('passwordnew','length', 'min'=>6, 'max'=>64),
			array('password, passwordnew', 'required'),
			// email has to be a valid email address
			//array('passwordnew','compare','compareAttribute'=>'passwordconf','operator'=>'!=','message'=>'Confirmación de clave no es igual'),
                        array('passwordnew', 'passwordStrength', 'strength'=>self::STRONG),
                        array('passwordconf','compare','compareAttribute'=>'passwordnew','message'=>'Confirmación de clave no es igual'),
                        array('passwordconf', 'safe')
		);
	}

        public function passwordStrength($attribute,$params)
        {
            if ($params['strength'] === self::WEAK)
                $pattern = '/^(?=.*[a-zA-Z0-9]).{5,}$/';  
            elseif ($params['strength'] === self::STRONG)
                $pattern = '/^(?=.*\d(?=.*\d))(?=.*[a-zA-Z](?=.*[a-zA-Z])).{5,}$/';  

            if(!preg_match($pattern, $this->$attribute))
              $this->addError($attribute, 'Su clave no cumple con el requerimiento minimo de complejidad');
        }
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
                        'password'=>'Clave Actual',
                        'passwordnew'=>'Nueva Clave',
			'passwordconf'=>'Confirmación',
		);
	}
}