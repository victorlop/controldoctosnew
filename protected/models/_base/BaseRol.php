<?php

/**
 * This is the model base class for the table "rol".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Rol".
 *
 * Columns in table "rol" available as properties of the model,
 * followed by relations of table "rol" available as properties of the model.
 *
 * @property string $id
 * @property string $roldescription
 * @property integer $active
 * @property integer $usedashboard
 *
 * @property Rolstatus[] $rolstatuses
 * @property Rolsystemoption[] $rolsystemoptions
 * @property User[] $users
 */
abstract class BaseRol extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'rol';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Rol|Rols', $n);
	}

	public static function representingColumn() {
		return 'roldescription';
	}

	public function rules() {
		return array(
			array('roldescription', 'required'),
			array('active, usedashboard', 'numerical', 'integerOnly'=>true),
			array('roldescription', 'length', 'max'=>50),
			array('active, usedashboard', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, roldescription, active, usedashboard', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'rolstatuses' => array(self::HAS_MANY, 'Rolstatus', 'rolid'),
			'rolsystemoptions' => array(self::HAS_MANY, 'Rolsystemoption', 'rolid'),
			'users' => array(self::HAS_MANY, 'User', 'rolid'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'roldescription' => Yii::t('app', 'Descripción'),
			'active' => Yii::t('app', 'Active'),
                        'usedashboard' => Yii::t('app', 'Dash Board'),
			'rolstatuses' => null,
			'rolsystemoptions' => null,
			'users' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('roldescription', $this->roldescription, true);
		$criteria->compare('active', 1);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
		));
	}
}