<?php

/**
 * This is the model base class for the table "projectflowhistory".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Projectflowhistory".
 *
 * Columns in table "projectflowhistory" available as properties of the model,
 * followed by relations of table "projectflowhistory" available as properties of the model.
 *
 * @property string $id
 * @property string $entrydate
 * @property string $userid
 * @property string $projectid
 * @property string $statusid
 *
 * @property Project $project
 * @property Status $status
 */
abstract class BaseProjectflowhistory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'projectflowhistory';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Projectflowhistory|Projectflowhistories', $n);
	}

	public static function representingColumn() {
		return 'entrydate';
	}

	public function rules() {
		return array(
			array('userid, projectid, statusid', 'required'),
			array('userid, projectid, statusid', 'length', 'max'=>20),
			array('entrydate', 'safe'),
			//array('entrydate', 'default', 'setOnEmpty' => true, 'value' => null),
                        array('entrydate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('id, entrydate, userid, projectid, statusid', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'projectid'),
			'status' => array(self::BELONGS_TO, 'Status', 'statusid'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'entrydate' => Yii::t('app', 'Entrydate'),
			'userid' => Yii::t('app', 'Userid'),
			'projectid' => null,
			'statusid' => null,
			'project' => null,
			'status' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('entrydate', $this->entrydate, true);
		$criteria->compare('userid', $this->userid, true);
		$criteria->compare('projectid', $this->projectid);
		$criteria->compare('statusid', $this->statusid);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}