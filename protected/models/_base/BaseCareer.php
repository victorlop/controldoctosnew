<?php

/**
 * This is the model base class for the table "career".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Career".
 *
 * Columns in table "career" available as properties of the model,
 * followed by relations of table "career" available as properties of the model.
 *
 * @property string $id
 * @property string $careerdescription
 * @property string $comments
 * @property string $code
 * @property string $contactInfo
 * @property string $email
 *
 * @property Student[] $students
 */
abstract class BaseCareer extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'career';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Career|Careers', $n);
	}

	public static function representingColumn() {
		return 'careerdescription';
	}

	public function rules() {
		return array(
			array('careerdescription, comments, code', 'required'),
			array('careerdescription', 'length', 'max'=>256),
                        array('email', 'length', 'max'=>1024),
			array('code', 'length', 'max'=>10),
                        array('contactInfo', 'safe'),
                        array('contactInfo', 'default', 'setOnEmpty' => true, 'value' => null),
                        array('email', 'email','checkMX'=>true,'message'=>'Direccion de eMail invalida'),
			array('id, careerdescription, comments, code', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'students' => array(self::HAS_MANY, 'Student', 'CareerId'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'careerdescription' => Yii::t('app', 'Carrera'),
			'comments' => Yii::t('app', 'Comentarios'),
			'code' => Yii::t('app', 'Código'),
                        'contactInfo' => Yii::t('app', 'Contacto'),
                        'email' => Yii::t('app', 'Email'),
			'students' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;
		$criteria->compare('careerdescription', $this->careerdescription, true);
		$criteria->compare('comments', $this->comments, true);
		$criteria->compare('code', $this->code, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
		));
	}
}