<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uploadFiles
 *
 * @author FamLopBar
 */
class uploadFiles {
    //put your code here
    public function onFileUploaded($fullFileName,$userdata) {
        // userdata es el mismo valor que pusiste en config/main
        // fullFileName es la ruta del archivo listo para leer.
        $model = new Document;
        if ($userdata->requestid)
        {
            $model->requestid = $userdata->requestid;
            $newPath = 'uploadFiles/request/' . $userdata->requestid . '/' . substr($fullFileName,12);
        }
        
        move_uploaded_file($fullFileName, $newPath);

        $model->documentname = substr($fullFileName,12);
        $model->filename = $newPath;
        
        $model->save();
      
    }
}
