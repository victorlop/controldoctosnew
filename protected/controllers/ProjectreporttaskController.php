<?php

class ProjectreporttaskController extends GxController {

    
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Projectreporttask'),
		));
	}

	public function actionCreate($id) {
		$model = new Projectreporttask;
                $model->projectid = $id;
                
                $this->performAjaxValidation($model);
                $prj = Project::model()->findByPk($id);
                if ($prj->completed==1)
                {
                    //throw new CHttpException(404, 'Máximo de Temas ha sido alcanzado.');
                    EQuickDlgs::render('/site/error',array('code'=>654,'message'=>'Documento del Proyecto ha sido Completado'));
                    Yii::app()->end();
                }
                
		if (isset($_POST['Projectreporttask'])) {
			$model->setAttributes($_POST['Projectreporttask']);
                        $model->dateentry = date('Y-m-d', strtotime($model->dateentry));

                        $transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            $cnt = Projectreporttask::model()->count('projectid=:projectid and dateentry >=:dateentry',array(':projectid'=>$model->projectid,':dateentry'=>$model->dateentry));
                            if ($cnt > 0)
                            {
                                Yii::app()->user->setFlash('error','Fecha de Reporte no puede ser menor a las ya Ingresadas.');
                                EQuickDlgs::render('create',array('model'=>$model));
                                Yii::app()->end();
                            }

                            if ($model->save()) {
                            
                                if ($model->completed == 1)
                                {
                                    $model->project->completed = 1;
                                    $model->project->save();
                                }
                                
                                $fec = new DateTime($model->dateentry);
                                $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                                if (!$uploadedFile)
                                {
                                        Yii::app()->user->setFlash('error','Archivo invalido.');
                                        EQuickDlgs::render('create',array('model'=>$model));
                                        Yii::app()->end();
                                }
                                else
                                {
                                    $fileName = "{$uploadedFile}"; // random number + file name
                                    $docto = new Document;
                                    $docto->projectreporttaskid = $model->id;
                                    $docto->support = 0;
                                    $docto->documentdescription = 'Reporte avance - '.$fec->format('d/m/Y').' - '.$model->project->projectdescription;
                                    $docto->documentname = 'Reporte avance - '.$fec->format('d/m/Y').' - '.$model->project->projectdescription;
                                    $docto->filename =  $fileName;
                                    $docto->save();

                                    $uploadPath = Yii::app()->params['uploadPath'];  
                                    $newPath =$uploadPath. '/project';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/project/'.$model->projectid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/project/'.$model->projectid.'/Avances';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $uploadedFile->saveAs($newPath.'/['.$model->id.']'.$docto->filename); // image will uplode to rootDirectory/uploads/fiels/    
                                }
                            }
                            $transaction->commit();
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));


                        }
                        catch (Exception $e)
                        {
                            $transaction->rollBack();
                            Yii::app()->user->setFlash('error',$e->getMessage());
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Projectreporttask');


		if (isset($_POST['Projectreporttask'])) {
			$model->setAttributes($_POST['Projectreporttask']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                        $prjtask = Projectreporttask::model()->findByPk($id);
                        $tran = $prjtask->dbConnection->beginTransaction();
                        try
                        {
                            $uploadPath = Yii::app()->params['uploadPath'];
                            $docto = Document::model()->find('projectreporttaskid=:id',array(':id'=>$id));
                            $newPath =$uploadPath. '/project/'.$prjtask->projectid.'/Avances/['.$prjtask->id.']'.$docto->filename;
                            
                            $prjtask->delete();
                            $docto->delete();
                            
                            if (file_exists($newPath))
                                unlink ($newPath);
                        
                            if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
                            
                            $tran->commit();
                        }
                        catch (Exception $e)
                        {
                            $tran->rollBack();
                            throw new CHttpException($e->getCode(), $e->getMessage());
			}
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Projectreporttask');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
		$child_model = new Projectreporttask("searchByParent");
                 
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('admin',array(
                                    'model'=>$this->loadModel($id,'Project'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
	}

}

//http://www.yiiframework.com/wiki/192/config-use-ms-sql-server-2005-2008/
//