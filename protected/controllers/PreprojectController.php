<?php

class PreprojectController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','documents','lastentries'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Preproject')));
	}

	public function actionCreate() {
		$model = new Preproject;
                $this->performAjaxValidation($model);

		if (isset($_POST['Preproject'])) {
			$model->setAttributes($_POST['Preproject']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preproject');
                $flowid = $model->flowdoctoid;
                $doctoReady = $model->documentsready;
                
                $this->performAjaxValidation($model);
                
		if (isset($_POST['Preproject'])) {
			$model->setAttributes($_POST['Preproject']);
                        if (DocumentsFlowOperation::CanModifyPreProject($model));
                        {
                            if ($flowid!=$model->flowdoctoid)
                                $model->statusid=  DocumentsFlowOperation::InitialFlowStatus($model->flowdoctoid);

                            if (!$doctoReady && $model->documentsready)
                            {
                                $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
                                $criteria->condition = 'flowid=:flwid and currentstatusid=:statusid';
                                $criteria->params=array(':flwid'=>$flowid,':statusid'=>$model->statusid);

                                $flowdet= FlowdoctoDetail::model()->find($criteria);
                                if ($flowdet) {
                                    $model->statusid = $flowdet->newstatusid;
                                    
                                    $flHst = new Preprojectflowhistory;
                                    $flHst->preprojectid = $model->id;
                                    $flHst->statusid = $model->statusid;
                                    $flHst->userid = Yii::app()->user->GetState('id');
                                }
                            }
                        }

			if ($model->save()) {
                            
                            if ($flHst)
                                $flHst->save();
                            
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Preproject');
            
            if (!$row->status->allowinactivation)
                throw new CHttpException(404, 'Estado invalido para operacion.');
            
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Preproject('search');
		$model->unsetAttributes();

		if (isset($_GET['Preproject']))
			$model->setAttributes($_GET['Preproject']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Preproject('search');
		$model->unsetAttributes();

		if (isset($_GET['Preproject']))
			$model->setAttributes($_GET['Preproject']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
            public function actiondocuments($id) {
                 $child_model = new Document("searchByPreProject");
                 
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('documents',array(
                                    'model'=>$this->loadModel($id,'PreProject'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
        
        public function actionLastentries() {
            $model = new Preproject('lastentries');
            $model->unsetAttributes();

            if (isset($_GET['Preproject']))
                    $model->setAttributes($_GET['Preproject']);

            $this->render('lastentries', array(
                    'model' => $model,
            ));
        }    
}