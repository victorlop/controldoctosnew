<?php

class ProjectreviewController extends GxController {
    
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Projectreview'),
		));
	}

	public function actionCreate($id) {
		$model = new Projectreview;
                $model->projectid= $id;

                $prj = Project::model()->findByPk($id);
                if ($prj)
                    $model->statusid = $prj->statusId;
                
                $this->performAjaxValidation($model);
                
                if (!DocumentsFlowOperation::CanModifyProjectMaxReviewers($prj))
                {
                    //throw new CHttpException(404, 'Máximo de Temas ha sido alcanzado.');
                    EQuickDlgs::render('/site/error',array('code'=>654,'message'=>'Máximo de Revisores ha sido alcanzado.'));
                    Yii::app()->end();
                }
                
		if (isset($_POST['Projectreview'])) {
                    
                    
			$model->setAttributes($_POST['Projectreview']);
                        $model->ended=0;
                        $model->approved=0;
                        
			if ($model->save()) {
                            
                            if ($prj->firstreview==0)
                            {
                                $prj->minreviewerid = $model->id;
                                $prj->firstreview = 1;
                                $prj->save();
                            }

                            try
                            {
                                $envio = AppParameterOperation::EnableValue('MAILREV');
                                if ($envio==1)
                                    EmailProcess::SendMailAsignProjectReview($model->id);
                            }
                            catch (Exception $e)
                            {
                            
                            }
                            
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));	
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Projectreview');


		if (isset($_POST['Projectreview'])) {
			$model->setAttributes($_POST['Projectreview']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Projectreview')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Projectreview');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
                $pend = PendingsProcess::model()->findByPk($id);   
            
                $id = ($id-$pend->TipoId) / ($pend->TipoId * 1000000);
		$model = new Projectreview('searchByParent');
		$model->unsetAttributes();
                $model_parent = Project::model()->findByPk($id);

		if (isset($_GET['Projectreview']))
			$model->setAttributes($_GET['Projectreview']);

		$this->render('admin', array(
			'model' => $model,
                         'model_parent' => $model_parent,
                        'parentId' => $id,
		));
	}

}