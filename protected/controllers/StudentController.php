<?php

class StudentController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
            EQuickDlgs::render('view',array('model'=>$this->loadModel($id, 'Student')));
	}

	public function actionCreate() {
		$model = new Student;
                $this->performAjaxValidation($model);               

		if (isset($_POST['Student'])) {
			$model->setAttributes($_POST['Student']);
                        
                        $transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            $model->status=1;
                            $model->save();

                            $envio = AppParameterOperation::EnableValue('MAILSTU');
                            if ($envio==1)
                                EmailProcess::SendMailNewStudent($model->id);
                            
                            $transaction->commit();
                            
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
                        catch (Exception $e)
                        {
                            $transaction->rollBack();
                            Yii::app()->user->setFlash('error',$e->getMessage());
                        }
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Student');
                $this->performAjaxValidation($model);

		if (isset($_POST['Student'])) {
			$model->setAttributes($_POST['Student']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Student');
            $row->status=0;
            $row->save();

            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            
	}

	public function actionIndex() {
		$model = new Student('search');
		$model->unsetAttributes();

		if (isset($_GET['Student']))
			$model->setAttributes($_GET['Student']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Student('search');
		$model->unsetAttributes();

		if (isset($_GET['Student']))
			$model->setAttributes($_GET['Student']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}