<?php

class PreprojectreviewlogController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Preprojectreviewlog'),
		));
	}

	public function actionCreate() {
		$model = new Preprojectreviewlog;


		if (isset($_POST['Preprojectreviewlog'])) {
			$model->setAttributes($_POST['Preprojectreviewlog']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preprojectreviewlog');


		if (isset($_POST['Preprojectreviewlog'])) {
			$model->setAttributes($_POST['Preprojectreviewlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Preprojectreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Preprojectreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
              
                $pend = PendingsProcess::model()->findByPk($id);
                
                //$id = $id / ($pend->TipoId * 2000000);
                $id = ($id - $pend->TipoId) / ($pend->TipoId * 1000000);   
                $model = new Preprojectreviewlog('searchByParent');
		$model->unsetAttributes();

		if (isset($_GET['Preprojectreviewlog']))
			$model->setAttributes($_GET['Preprojectreviewlog']);

		$this->render('admin', array(
			'model' => $model,
                        'parentId' => $id,
		));
	}

}