<?php

class PreprojectreviewerlogController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Preprojectreviewerlog'),
		));
	}

	public function actionCreate($id) {
		$model = new Preprojectreviewerlog;
                $model->preprojectreviewerid = $id;
                
                $this->performAjaxValidation($model);
		if (isset($_POST['Preprojectreviewerlog'])) {
			$model->setAttributes($_POST['Preprojectreviewerlog']);
                        $transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            
                            if ($model->save()) {
                                $fec = new DateTime();
                                
                                $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                                if ($uploadedFile)
                                {
                                    $fileName = "{$uploadedFile}"; // random number + file name
                                    $docto = new Document;
                                    $docto->preprojectreviewerlogid = $model->id;
                                    $docto->support = 0;
                                    $docto->documentdescription = 'Revision - '.$fec->format('d/m/Y H:i:s').' - '.$model->preprojectreviewer->teacher->teachername;
                                    $docto->documentname = 'Revision - '.$fec->format('d/m/Y H:i:s').' - '.$model->preprojectreviewer->teacher->teachername;
                                    $docto->filename =  $fec->format('Ymdhis').'_'.$fileName;
                                    $docto->save();
                                    
                                    $uploadPath = Yii::app()->params['uploadPath'];  
                                            
                                    $newPath =$uploadPath. '/preproject';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/preproject/'.$model->preprojectreviewer->preprojectid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/preproject/'.$model->preprojectreviewer->preprojectid.'/'.$model->preprojectreviewer->statusid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);
                                    
                                    $newPath =$uploadPath. '/preproject/'.$model->preprojectreviewer->preprojectid.'/'.$model->preprojectreviewer->statusid.'/'.$model->id;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $uploadedFile->saveAs($newPath.'/['.$model->id.']'.$docto->filename); // image will uplode to rootDirectory/uploads/fiels/    
                                }
                            }
                            
                            $transaction->commit();
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
                        }
                        catch (Exception $e)
                        {
                            Yii::app()->user->setFlash('error',$e->getMessage());
                            //throw new CHttpException(825, Yii::t('app', $e->getMessage()));
                        }
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preprojectreviewerlog');


		if (isset($_POST['Preprojectreviewerlog'])) {
			$model->setAttributes($_POST['Preprojectreviewerlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Preprojectreviewerlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Preprojectreviewerlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
            
                $id = ($id - (int)Yii::app()->user->GetState("id")*2000000) * 2000000 + 2;
                $pendrev = PendingsReviews::model()->find('reviewid=:reviewid',array(':reviewid'=>$id));
                
                $id = ($id - $pendrev->TipoId) / ($pendrev->TipoId * 1000000);   

		$model = new Preprojectreviewerlog('searchByParent');
		$model->unsetAttributes();
                
                $model_parent = Preprojectreviewer::model()->findByPk($id);
                

		if (isset($_GET['Preprojectreviewerlog']))
			$model->setAttributes($_GET['Preprojectreviewerlog']);

		$this->render('admin', array(
			'model' => $model,
                        'model_parent' => $model_parent,
                        'parentId' => $id
		));
	}

}