<?php

class PreprojectreviewerController extends GxController {
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Preprojectreviewer'),
		));
	}

	public function actionCreate($id) {
		$model = new Preprojectreviewer;
                
                $model->preprojectid= $id;
                
                $preprj = Preproject::model()->findByPk($id);
                if ($preprj)
                    $model->statusid = $preprj->statusid;
                
                $this->performAjaxValidation($model);
                
                if (!DocumentsFlowOperation::CanModifyPreProjectMaxReviewers($preprj))
                {
                    //throw new CHttpException(404, 'Máximo de Temas ha sido alcanzado.');
                    EQuickDlgs::render('/site/error',array('code'=>654,'message'=>'Máximo de Revisores ha sido alcanzado.'));
                    Yii::app()->end();
                }
                
		if (isset($_POST['Preprojectreviewer'])) {
			$model->setAttributes($_POST['Preprojectreviewer']);
                        $model->ended=0;
                        $model->approved=0;
			if ($model->save()) {
                            
                            if ($preprj->firstreview==0)
                            {
                                $preprj->minreviewerid = $model->id;
                                $preprj->firstreview = 1;
                                $preprj->save();
                            }

                            try
                            {
                                $envio = AppParameterOperation::EnableValue('MAILREV');
                                if ($envio==1)
                                    EmailProcess::SendMailAsignPreProjectReview($model->id);
                            }
                            catch (Exception $e)
                            {
                            
                            }
                            
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));			
                        }
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preprojectreviewer');

		if (isset($_POST['Preprojectreviewer'])) {
			$model->setAttributes($_POST['Preprojectreviewer']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Preprojectreviewer')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Preprojectreviewer');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {

                $pend = PendingsProcess::model()->findByPk($id);                
                $id = $id - $pend->TipoId;
                $id = $id / ($pend->TipoId * 1000000);
                
                $model = new Preprojectreviewer('searchByParent');
		$model->unsetAttributes();
                $model_parent = Preproject::model()->findByPk($id);

		if (isset($_GET['Preprojectreviewlog']))
			$model->setAttributes($_GET['Preprojectreviewlog']);

		$this->render('admin', array(
			'model' => $model,
                        'model_parent' => $model_parent,
                        'parentId' => $id,
		));
	}

}