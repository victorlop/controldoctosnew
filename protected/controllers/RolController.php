<?php

class RolController extends GxController {

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','child','status'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		/*$this->render('view', array(
			'model' => $this->loadModel($id, 'Rol'),
		));*/
            EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Rol')));
	}

	public function actionCreate() {
		$model = new Rol;

                $this->performAjaxValidation($model);
		if (isset($_POST['Rol'])) {
			$model->setAttributes($_POST['Rol']);
                        $model->active=1;
                        
			if ($model->save()) {
			/*	if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}*/
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
                        }
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Rol');
                $this->performAjaxValidation($model);

		if (isset($_POST['Rol'])) {
			$model->setAttributes($_POST['Rol']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

                EQuickDlgs::render('update',array('model'=>$model));
                
		/*$this->render('update', array(
				'model' => $model,
				));*/
	}

	public function actionDelete($id) {
	
            $row=  $this->loadModel($id,'Rol');
            $cuantos = User::model()->count('status=1 and rolid=:rolid',array(':rolid'=>$id));

            if ($cuantos>0)
                throw new CHttpException(404, 'Rol tiene usuarios activos.');
                
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Rol('search');
		$model->unsetAttributes();

		if (isset($_GET['Rol']))
			$model->setAttributes($_GET['Rol']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Rol('search');
		$model->unsetAttributes();

		if (isset($_GET['Rol']))
			$model->setAttributes($_GET['Rol']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionChild($id) {
                 $child_model = new Rolsystemoption("searchByParent");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('child',array(
                                    'model'=>$this->loadModel($id,'Rol'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
        public function actionStatus($id) {
                 $child_model = new Rolstatus("searchByParent");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('status',array(
                                    'model'=>$this->loadModel($id,'Rol'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
}