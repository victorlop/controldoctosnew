<?php

class PendingsProcessController extends GxController {

     public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'PendingsProcess'),
		));
	}

	public function actionCreate() {
		$model = new PendingsProcess;


		if (isset($_POST['PendingsProcess'])) {
			$model->setAttributes($_POST['PendingsProcess']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'PendingsProcess');
                $this->performAjaxValidation($model);
                $id = $id - $model->TipoId;
                $id = $id / ($model->TipoId * 1000000);

		if (isset($_POST['PendingsProcess'])) {
			$model->setAttributes($_POST['PendingsProcess']);
                        
                        $transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            $st=  Status::model()->findByPk((int)$model->statusid);
                            
                            switch ($model->TipoId)
                            {
                                case 1:
                                    $request = Request::model()->findByPk($id);
                                    $cuantos = Requestdetail::model()->count('requestid=:requestid',array(':requestid'=>$id));
                                    if ($cuantos!=DocumentsFlowOperation::RequestMaxTopic())
                                    {
                                        $model->statusid = $request->statusid;
                                        $model->id = $request->id * ($model->TipoId * 1000000) + $model->TipoId;
                                        throw new CHttpException(400, Yii::t('app', 'Solicitud con seleccion de Temas Incompleto.'));
                                    }

                                    if ($st->approve=="1")
                                    {
                                        if ($model->topicid=="")
                                        {
                                            $model->statusid = $request->statusid;
                                            $model->id = $request->id * ($model->TipoId * 1000000)+ $model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'Debe seleccionar el tema Autorizado.'));
                                        }
                                    }
                                    $cambioStatus = $request->statusid != $model->statusid ? TRUE:FALSE;
                                    
                                    $request->statusid = $model->statusid;
                                    
                                    if ($request->save())
                                    {                                        
                                        $requestflowhistory = new Requestflowhistory;
                                        $requestflowhistory->requestid = $request->id;
                                        $requestflowhistory->statusid = $request->statusid;
                                        $requestflowhistory->userid = Yii::app()->user->GetState('id');
                                        $requestflowhistory->save();
                                        
                                        $envio = AppParameterOperation::EnableValue('MAILSTU');
                                        if ($envio==1 && $cambioStatus)
                                            EmailProcess::SendMailRequestChangeStatus($request->id);
                                        
                                        if ($st->approve=="1")
                                        {
                                            $requestdetails = Requestdetail::model()->findAll('requestid=:reqid',array(':reqid'=>$request->id));
                                            foreach ($requestdetails as $reqDt)
                                            {
                                                if ($reqDt->topicid != $model->topicid )
                                                {
                                                    $topic = Topic::model()->findByPk($reqDt->topicid);
                                                    
                                                    
                                                    if ($topic) {
                                                        $topic->topicused = 0;
                                                        $topic->save();
                                                    }
                                                }
                                                else
                                                {
                                                    $reqDt->approved=1;
                                                    $reqDt->save();

                                                    $topic = Topic::model()->findByPk($reqDt->topicid);
                                                    
                                                    $request->topicid = $reqDt->topicid;
                                                    $request->save();
                                                    
                                                    $flwPre = DocumentsFlowOperation::PreProjectDefaultFlow();
                                                    $prePrj = new Preproject;
                                                    $prePrj->active = 1;
                                                    $prePrj->flowdoctoid = $flwPre->parametervalue;
                                                    $prePrj->statusid = DocumentsFlowOperation::PreProjectInitialFlowStatus();
                                                    $prePrj->preprojectdescription = $topic->topicdescription;
                                                    $prePrj->studentid = $request->studentid;
                                                    $prePrj->comments = $request->comment;
                                                    $prePrj->requestdetailid = $reqDt->id;
                                                    $prePrj->minreviewerid = 0;
                                                    $prePrj->firstreview=0;
                                                    $prePrj->save();

                                                    $prePrjHst= new Preprojectflowhistory;
                                                    $prePrjHst->preprojectid = $prePrj->id;
                                                    $prePrjHst->statusid = $prePrj->statusid;
                                                    $prePrjHst->userid = Yii::app()->user->GetState('id');
                                                    $prePrjHst->save();
                                                    
                                                    $envio = AppParameterOperation::EnableValue('MAILSTU');
                                                    if ($envio==1 && $cambioStatus)
                                                        EmailProcess::SendMailPreProjectChangeStatus($prePrj->id);
                                                }
                                            }
                                        }
                                        else if ($st->freetopic)
                                        {
                                            $requestdetails = Requestdetail::model()->findAll('requestid=:reqid',array(':reqid'=>$request->id));
                                            foreach ($requestdetails as $reqDt)
                                            {
                                                $reqDt->topic->topicused=0;
                                                $reqDt->topic->save();
                                                /*$topic = Topic::model()->findByPk($reqDt->topicid);

                                                if ($topic) {
                                                    $topic->topicused = 0;
                                                    $topic->save();
                                                }*/
                                            }
                                        }    
                                    }
                                    break;
                                case 2:
                                    $preprj = Preproject::model()->findByPk($id);
                                    //Verifico Estado requiere revisores
                                    $currentStatus = Status::model()->findByPk((int)$preprj->statusid);
                                    if ($currentStatus->amountreviews>0 && 
                                            $preprj->statusid != $model->statusid)
                                    {
                                        $cuantos = Preprojectreviewer::model()->count('preprojectid=:preprojectid and statusid=:statusid and ended=:ended and id>=:minrev',
                                                                                    array(':preprojectid'=>$id,':statusid'=>$currentStatus->id,':ended'=>1,':minrev'=>(int)$preprj->minreviewerid));
                                        //$cuantos = User::model()->count('status=1 and rolid=:rolid',array(':rolid'=>$id));
                                        if ($cuantos!=$currentStatus->amountreviews) 
                                        {
                                            $model->statusid = $preprj->statusid;
                                            $model->id = $preprj->id * ($model->TipoId * 1000000)+$model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'Existen revisiones Pendientes.'));
                                        }
                                    }
                                    
                                    $preprj->statusid = $model->statusid;
                                    if ($preprj->statusid != $currentStatus->id)
                                        $preprj->firstreview=0;
                                  
                                    $cambioStatus=FALSE;
                                    
                                    if ($preprj->save())
                                    {
                                        
                                        if ($preprj->statusid != $currentStatus->id)
                                        {
                                            $preprojecthistory = new Preprojectflowhistory;
                                            $preprojecthistory->preprojectid = $preprj->id;
                                            $preprojecthistory->statusid = $preprj->statusid;
                                            $preprojecthistory->userid = Yii::app()->user->GetState('id');
                                            $preprojecthistory->save();
                                            
                                            $cambioStatus = TRUE;
                                        }
                                        
                                        $envio = AppParameterOperation::EnableValue('MAILSTU');
                                        if ($envio==1 && $cambioStatus)
                                            EmailProcess::SendMailPreProjectChangeStatus($preprj->id);
                                        
                                        $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                                        if ($uploadedFile)
                                        {
                                            $dt = new DateTime();
                                            $fileName = "{$uploadedFile}"; // random number + file name
                                            
                                            $docto = new Document;
                                            $docto->preprojectid = $preprj->id;
                                            $docto->statusid =$model->statusid;
                                            $docto->support = 0;
                                            $docto->documentdescription = 'Revision - '.$preprj->preprojectdescription;
                                            $docto->documentname = 'Revision - '.$preprj->preprojectdescription;
                                            $docto->filename = $dt->format('Ymdhis').'_'.$fileName;
                                            $docto->save();
                                            
                                            $uploadPath = Yii::app()->params['uploadPath'];  
                                            
                                            $newPath =$uploadPath. '/preproject';
                                            if (!file_exists($newPath))
                                                mkdir($newPath);

                                            $newPath =$uploadPath. '/preproject/'.$preprj->id;
                                            if (!file_exists($newPath))
                                                mkdir($newPath);
                                            
                                            $newPath =$uploadPath. '/preproject/'.$preprj->id.'/'.$model->statusid;
                                            if (!file_exists($newPath))
                                                mkdir($newPath);

                                            $uploadedFile->saveAs($newPath.'/['.$preprj->id.']'.$docto->filename); // image will uplode to rootDirectory/uploads/fiels/    

                                        }
                                        
                                        if ($st->approve=="1")
                                        {
                                            $flwPro = DocumentsFlowOperation::ProjectDefaultFlow();
                                            $prj = new Project;
                                            $prj->active = 1;
                                            $prj->flowdoctoid = $flwPro->parametervalue;
                                            $prj->statusId = DocumentsFlowOperation::ProjectInitialFlowStatus();
                                            $prj->projectdescription = $preprj->preprojectdescription;
                                            $prj->studentId = $preprj->studentid;
                                            $prj->comments = $preprj->comments;
                                            $prj->preprojectid = $preprj->id;
                                            $prj->minreviewerid = 0;
                                            $prj->firstreview=0;
                                            $prj->save();
                                            
                                            $PrjHst= new Projectflowhistory;
                                            $PrjHst->projectid = $prj->id;
                                            $PrjHst->statusid = $prj->statusId;
                                            $PrjHst->userid = Yii::app()->user->GetState('id');
                                            $PrjHst->save();
                                            
                                            $envio = AppParameterOperation::EnableValue('MAILSTU');
                                            if ($envio==1 && $cambioStatus)
                                                EmailProcess::SendMailProjectChangeStatus($prj->id);
                                        }
                                      }
                                    break;
                                case 3:
                                    $prj = Project::model()->findByPk($id);
                                    //Verifico Estado requiere revisores
                                    $currentStatus = Status::model()->findByPk((int)$prj->statusId);
                                    if ($currentStatus->amountreviews>0 && 
                                            $prj->statusId != $model->statusid)
                                    {
                                        $cuantos = Projectreview::model()->count('projectid=:projectid and statusId=:statusid and ended=:ended and id>=:minrev',
                                                                                    array(':projectid'=>$id,':statusid'=>$currentStatus->id,':ended'=>1,':minrev'=>(int)$prj->minreviewerid));
                                        //$cuantos = User::model()->count('status=1 and rolid=:rolid',array(':rolid'=>$id));
                                        if ($cuantos!=$currentStatus->amountreviews) 
                                        {
                                            $model->statusid = $prj->statusId;
                                            $model->id = $prj->id * ($model->TipoId * 1000000)+$model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'Existen revisiones Pendientes.'));
                                        }
                                    }
                                    
                                    $prj->statusId = $model->statusid;
                                    if ($prj->statusId != $currentStatus->id)
                                        $prj->firstreview=0;
                                  
                                    $cuantos = Projectreview::model()->count('projectid=:projectid and statusId=:statusid and ended=:ended and id>=:minrev',
                                                                              array(':projectid'=>$id,':statusid'=>$prj->statusId,':ended'=>1,':minrev'=>(int)$prj->minreviewerid));
                                                                               
                                    if ($prj->save())
                                    {
                                        $cambioStatus=FALSE;
                                        if ($prj->statusId != $currentStatus->id)
                                        {
                                            $projecthistory = new Projectflowhistory;
                                            $projecthistory->projectid = $prj->id;
                                            $projecthistory->statusid = $prj->statusId;
                                            $projecthistory->userid = Yii::app()->user->GetState('id');
                                            $projecthistory->save();
                                            
                                            $cambioStatus = TRUE;
                                        }
                                        
                                        $envio = AppParameterOperation::EnableValue('MAILSTU');
                                        //if ($envio==1 && $cambioStatus)
                                        //    EmailProcess::SendMailProjectChangeStatus($prj->id);
                                            
                                        $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                                        
                                        if ($prj->statusId != $currentStatus->id && !$uploadedFile && $prj->status->amountreviews>0)
                                        {
                                            $model->statusid = $currentStatus->id;
                                            $model->id = $prj->id * ($model->TipoId * 1000000)+$model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'Debe de seleccionar documento a Revisar.'));
                                        }
                                        if ($uploadedFile)
                                        {
                                            if ($cuantos>0)
                                            {
                                                $model->statusid = $currentStatus->id;
                                                $model->id = $prj->id * ($model->TipoId * 1000000)+$model->TipoId;
                                                throw new CHttpException(400, Yii::t('app', 'Ya se encuentran asignados Revisores.'));
                                            }
                                            $dt = new DateTime();
                                            $fileName = "{$uploadedFile}"; // random number + file name
                                            
                                            $docto = new Document;
                                            $docto->projectid = $prj->id;
                                            $docto->statusid =$model->statusid;
                                            $docto->support = 0;
                                            $docto->documentdescription = 'Revision - '.$prj->projectdescription;
                                            $docto->documentname = 'Revision - '.$prj->projectdescription;
                                            $docto->filename = $dt->format('Ymdhis').'_'.$fileName;
                                            $docto->save();
                                            
                                            $uploadPath = Yii::app()->params['uploadPath'];  
                                            
                                            $newPath =$uploadPath. '/project';
                                            if (!file_exists($newPath))
                                                mkdir($newPath);

                                            $newPath =$uploadPath. '/project/'.$prj->id;
                                            if (!file_exists($newPath))
                                                mkdir($newPath);
                                            
                                            $newPath =$uploadPath. '/project/'.$prj->id.'/'.$model->statusid;
                                            if (!file_exists($newPath))
                                                mkdir($newPath);

                                            $uploadedFile->saveAs($newPath.'/['.$prj->id.']'.$docto->filename); // image will uplode to rootDirectory/uploads/fiels/    

                                        }
                                        if ($envio==1 && $cambioStatus)
                                            EmailProcess::SendMailProjectChangeStatus($prj->id);
                                        if ($st->approve=="1")
                                        {
                                            
                                        }
                                    }
                                    break;
                            }
                            $transaction->commit();
                            
                            $model->id = $model->id / ($model->TipoId * 1000000)+$model->TipoId;
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
                        }
                        catch (Exception $e)
                        {
                            $transaction->rollBack();
                            switch ($model->TipoId)
                            {
                                case 1:
                                    $child_model = new Requestreviewlog("searchByParent");
                                    break;
                                case 2:
                                    $child_model = new Preprojectreviewlog("searchByParent");
                                    break;
                                case 3:
                                    $child_model = new Projectreviewlog("searchByParent");
                                    break;
                            }
                            $child_model->unsetAttributes();
                            $model->id = $model->id - $model->TipoId;
                            $model->id = $model->id / ($model->TipoId * 1000000);
                            
                            Yii::app()->user->setFlash('error',$e->getMessage());
                            /*EQuickDlgs::render('update',array('model'=>$model,
                                                                'child_model'=>$child_model,
                                                                'parentId' => $model->id));
                            Yii::app()->end();*/
                        }
		}
                
                switch ($model->TipoId)
                {
                    case 1:
                        $child_model = new Requestreviewlog("searchByParent");
                        break;
                    case 2:
                        $child_model = new Preprojectreviewlog("searchByParent");
                        break;
                    case 3:
                        $child_model = new Projectreviewlog("searchByParent");
                        break;
                }
                $child_model->unsetAttributes();
                
                EQuickDlgs::render('update',array('model'=>$model,
                                                  'child_model'=>$child_model,
                                                  'parentId' => $id));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PendingsProcess')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		
		$model = new PendingsProcess('search');
		$model->unsetAttributes();

		if (isset($_GET['PendingsProcess']))
			$model->setAttributes($_GET['PendingsProcess']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
            
		$model = new PendingsProcess('search');
		$model->unsetAttributes();

		if (isset($_GET['PendingsProcess']))
			$model->setAttributes($_GET['PendingsProcess']);

		$this->render('admin', array(
			'model' => $model,
                        'parentId' => $id,
		));
	}
        

}