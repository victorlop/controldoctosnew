<?php

class FlowdoctoController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','child','order'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Flowdocto')));
	}

	public function actionCreate() {
		$model = new Flowdocto;
                $this->performAjaxValidation($model);

		if (isset($_POST['Flowdocto'])) {
			$model->setAttributes($_POST['Flowdocto']);
                        $model->status=1;

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Flowdocto');
                $this->performAjaxValidation($model);    

		if (isset($_POST['Flowdocto'])) {
			$model->setAttributes($_POST['Flowdocto']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}
                EQuickDlgs::render('update',array('model'=>$model));	
        }

	public function actionDelete($id) {

            $row= loadModel($id, 'Flowdocto')->delete();
            
            $cuantos = User::model()->count('status=1 and rolid=:rolid',array(':rolid'=>$id));

            if ($cuantos>0)
                throw new CHttpException(404, 'Rol tiene usuarios activos.');
            
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

	}

	public function actionIndex() {
		$model = new Flowdocto('search');
		$model->unsetAttributes();

		if (isset($_GET['Flowdocto']))
			$model->setAttributes($_GET['Flowdocto']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Flowdocto('search');
		$model->unsetAttributes();

		if (isset($_GET['Flowdocto']))
			$model->setAttributes($_GET['Flowdocto']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionChild($id) {
                 $child_model = new FlowdoctoDetail("searchByParent");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('child',array(
                                    'model'=>$this->loadModel($id,'Flowdocto'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
        
        public function actions()
        {
            return array(
              'order' => array(
                  'class' => 'ext.OrderColumn.OrderAction',
                  'modelClass' => 'FlowdoctoDetail',
                  'pkName'  => 'id',
                  ),
            );
        }


}