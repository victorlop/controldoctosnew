<?php

class DocumentController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','createrequest','createpreproject'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Document')));

	}

	public function actionCreate() {
		$model = new Document;


		if (isset($_POST['Document'])) {
			$model->setAttributes($_POST['Document']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Document');
                $this->performAjaxValidation($model);        

		if (isset($_POST['Document'])) {
			$model->setAttributes($_POST['Document']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));	
	}
        
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                        $docto = $this->loadModel($id, 'Document');
                            
			$docto->delete();
                        
                        $uploadPath = Yii::app()->params['uploadPath'];
                        $newPath =$uploadPath. '/request/'.$docto->requestid.'/['.$docto->id.']'.$docto->filename;
                        if (file_exists($newPath))
                            unlink ($newPath);

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Document');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Document('search');
		$model->unsetAttributes();

		if (isset($_GET['Document']))
			$model->setAttributes($_GET['Document']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

        public function actionCreaterequest($id) {
            $model = new Document;
            $model->requestid = $id;
            $this->performAjaxValidation($model);

		if (isset($_POST['Document'])) {
			$model->setAttributes($_POST['Document']);
                        
                        $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                        if (!$uploadedFile)
                        {
                            	Yii::app()->user->setFlash('error','Archivo invalido.');
                                EQuickDlgs::render('createrequest',array('model'=>$model));
                                Yii::app()->end();
                        }
                        
                        $fileName = "{$uploadedFile}"; // random number + file name
                        $uploadPath = Yii::app()->params['uploadPath'];
              
                        $model->filename = $fileName;
                        
			if ($model->save()) {
                            //Yii::app()->end();
                            
                            $newPath =$uploadPath. '/request';
                            if (!file_exists($newPath))
                                mkdir($newPath);
                            
                            $newPath =$uploadPath. '/request/'.$model->requestid;
                            if (!file_exists($newPath))
                                mkdir($newPath);
                                                        
                            $uploadedFile->saveAs($newPath.'/['.$model->id.']'.$fileName); // image will uplode to rootDirectory/uploads/fiels/    

                        
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

                EQuickDlgs::render('createrequest',array('model'=>$model));
            
        }
        public function actionCreatepreproject($id) {
            $model = new Document;
            $model->preprojectid = $id;
            $this->performAjaxValidation($model);

		if (isset($_POST['Document'])) {
			$model->setAttributes($_POST['Document']);
                        
                        $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                        if (!$uploadedFile)
                        {
                            	Yii::app()->user->setFlash('error','Archivo invalido.');
                                EQuickDlgs::render('createrequest',array('model'=>$model));
                                Yii::app()->end();
                        }
                        
                        $fileName = "{$uploadedFile}"; // random number + file name
                        $uploadPath = Yii::app()->params['uploadPath'];
              
                        $model->filename = $fileName;
                        
			if ($model->save()) {
                            //Yii::app()->end();
                            
                            $newPath =$uploadPath. '/preproject';
                            if (!file_exists($newPath))
                                mkdir($newPath);
                            
                            $newPath =$uploadPath. '/preproject/'.$model->preprojectid;
                            if (!file_exists($newPath))
                                mkdir($newPath);
                                                        
                            $uploadedFile->saveAs($newPath.'/['.$model->id.']'.$fileName); // image will uplode to rootDirectory/uploads/fiels/    

                        
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

                EQuickDlgs::render('createpreproject',array('model'=>$model));
            
        }
}