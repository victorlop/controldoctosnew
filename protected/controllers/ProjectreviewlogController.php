<?php

class ProjectreviewlogController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Projectreviewlog'),
		));
	}

	public function actionCreate($id) {
		$model = new Projectreviewlog;
                $model->projectreviewid = $id;
                
                $this->performAjaxValidation($model);

		if (isset($_POST['Projectreviewlog'])) {
			$model->setAttributes($_POST['Projectreviewlog']);

			$transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            
                            if ($model->save()) {
                                $fec = new DateTime();
                                
                                $uploadedFile = CUploadedFile::getInstance($model, 'filename');
                                if ($uploadedFile)
                                {
                                    $fileName = "{$uploadedFile}"; // random number + file name
                                    $docto = new Document;
                                    $docto->projectreviewlogid = $model->id;
                                    $docto->support = 0;
                                    $docto->documentdescription = 'Revision - '.$fec->format('d/m/Y H:i:s').' - '.$model->projectreview->teacher->teachername;
                                    $docto->documentname = 'Revision - '.$fec->format('d/m/Y H:i:s').' - '.$model->projectreview->teacher->teachername;
                                    $docto->filename =  $fec->format('Ymdhis').'_'.$fileName;
                                    $docto->save();
                                    
                                    $uploadPath = Yii::app()->params['uploadPath'];  
                                            
                                    $newPath =$uploadPath. '/project';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/project/'.$model->projectreview->projectid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/project/'.$model->projectreview->projectid.'/'.$model->projectreview->statusid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);
                                    
                                    $newPath =$uploadPath. '/project/'.$model->projectreview->projectid.'/'.$model->projectreview->statusid.'/'.$model->id;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $uploadedFile->saveAs($newPath.'/['.$model->id.']'.$docto->filename); // image will uplode to rootDirectory/uploads/fiels/    
                                }
                            }
                            
                            $transaction->commit();
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
                        }
                        catch (Exception $e)
                        {
                            Yii::app()->user->setFlash('error',$e->getMessage());
                            //throw new CHttpException(825, Yii::t('app', $e->getMessage()));
                        }
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Projectreviewlog');


		if (isset($_POST['Projectreviewlog'])) {
			$model->setAttributes($_POST['Projectreviewlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Projectreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Projectreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
		$pendrev = PendingsReviews::model()->findByPk($id);
                
                $id = ($id - $pendrev->TipoId) / ($pendrev->TipoId * 1000000); 
                
		$model = new Projectreviewlog('searchByParent');
		$model->unsetAttributes();
                
                $model_parent = Projectreview::model()->findByPk($id);
                

		if (isset($_GET['Projectreviewlog']))
			$model->setAttributes($_GET['Projectreviewlog']);

		$this->render('admin', array(
			'model' => $model,
                        'model_parent' => $model_parent,
                        'parentId' => $id
		));
	}

}