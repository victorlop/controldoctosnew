<?php

class ProjectController extends GxController {

    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','documents','lastentries','lastupdated'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Project')));
	}

	public function actionCreate() {
		$model = new Project;


		if (isset($_POST['Project'])) {
			$model->setAttributes($_POST['Project']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Project');
                $flowid = $model->flowdoctoid;
                $this->performAjaxValidation($model);

		if (isset($_POST['Project'])) {
			$model->setAttributes($_POST['Project']);
                        if (DocumentsFlowOperation::CanModifyPreProject($model));
                        {
                            if ($flowid!=$model->flowdoctoid)
                                $model->statusId=  DocumentsFlowOperation::InitialFlowStatus($model->flowdoctoid);

                        

                            if ($model->save()) {
                                EQuickDlgs::checkDialogJsScript();
                                $this->redirect(array('admin','id'=>$model->id));
                            }
                        }
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Project');
            
            if (!$row->status->allowinactivation)
                throw new CHttpException(404, 'Estado invalido para operacion.');
            
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Project('search');
		$model->unsetAttributes();

		if (isset($_GET['Project']))
			$model->setAttributes($_GET['Project']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Project('search');
		$model->unsetAttributes();

		if (isset($_GET['Project']))
			$model->setAttributes($_GET['Project']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionLastentries() {
            $model = new Project('lastentries');
            $model->unsetAttributes();

            if (isset($_GET['Project']))
                    $model->setAttributes($_GET['Project']);

            $this->render('lastentries', array(
                    'model' => $model,
            ));
        }    
        public function actionLastupdated() {
            $model = new Project('lastupdated');
            $model->unsetAttributes();

            if (isset($_GET['Project']))
                    $model->setAttributes($_GET['Project']);

            $this->render('lastentries', array(
                    'model' => $model,
            ));
        }   
}