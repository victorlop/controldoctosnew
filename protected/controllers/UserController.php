<?php

class UserController extends GxController {

       

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','changepass'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		/*$this->render('view', array(
			'model' => $this->loadModel($id, 'User'),
		));*/
            EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'User')));
	}

	public function actionCreate() {
		$model = new User;

                $this->performAjaxValidation($model);
		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);
                        
                        $cuantos = User::model()->count('username=:username',array(':username'=>$model->username));
                        if ($cuantos>0)
                        {
                            Yii::app()->user->setFlash('usererror','Usuario de inicio ya existe');
                        }
                        else
                        {
                            $length = 20;
                            $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
                            shuffle($chars);
                            $newpassword = implode(array_slice($chars, 0, $length));
                            $model->password = crypt($newpassword,trim($model->username));
                            $model->passwordnew = $newpassword;
                            $model->save();

                            $model->status=1;

                            if ($model->save()) {
                                
                                EmailProcess::SendMailNewUser($model->id);
                                $model->passwordnew = NULL;
                                $model->save();

                                EQuickDlgs::checkDialogJsScript();
                                $this->redirect(array('admin'));
                                    /*if (Yii::app()->getRequest()->getIsAjaxRequest())
                                            Yii::app()->end();
                                    else
                                            $this->redirect(array('view', 'id' => $model->id));*/
                            }
                        }
		}
                EQuickDlgs::render('create',array('model'=>$model));
		
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'User');
                $this->performAjaxValidation($model);

		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
				//$this->redirect(array('view', 'id' => $model->id));
			}
		}

                EQuickDlgs::render('update',array('model'=>$model));
                
		/*$this->render('update', array(
				'model' => $model,
				));*/
	}

	public function actionDelete($id) {
            $currentid=Yii::app()->user->GetState('id');
            if ($currentid === $id) {
                throw new CHttpException(404, 'Usuario activo no puede eliminarse.');
            }
            
            $row=  $this->loadModel($id,'User');
            $row->status=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            
		/*if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'User')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));*/
	}

	public function actionIndex() {
		/*$dataProvider = new CActiveDataProvider('User');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));*/
                $model = new User('search');
		$model->unsetAttributes();

		if (isset($_GET['User']))
			$model->setAttributes($_GET['User']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new User('search');
		$model->unsetAttributes();

		if (isset($_GET['User']))
			$model->setAttributes($_GET['User']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

        public function actionChangepass() {
                $model = new ChangePass;
                $this->performAjaxValidation($model);
		if (isset($_POST['ChangePass'])) {
			$model->setAttributes($_POST['ChangePass']);
                        
                        $id = Yii::app()->user->GetState('id');
        		$User = User::model()->findByPk($id);
                        
                        if($User->password!==crypt($model->password,$User->username))
                        {
                            Yii::app()->user->setFlash('changepasserror','Clave actual incorrecta');
                        }
                        else
                        {
                            
                            $User->password = crypt($model->passwordnew,$User->username);
                            
                            if ($User->save()) {
                                var_dump($User);
                                Yii::app()->user->setFlash('changepasssuccess','Clave ha sido cambiada satisfactoriamente');
                                
                                EQuickDlgs::checkDialogJsScript();
                                $this->refresh();

                            }
                        }
		}
                EQuickDlgs::render('changepass',array('model'=>$model));
		
	}
}