<?php

class ProjectdetailController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Projectdetail'),
		));
	}

	public function actionCreate() {
		$model = new Projectdetail;


		if (isset($_POST['Projectdetail'])) {
			$model->setAttributes($_POST['Projectdetail']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Projectdetail');


		if (isset($_POST['Projectdetail'])) {
			$model->setAttributes($_POST['Projectdetail']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Projectdetail')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Projectdetail');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Projectdetail('search');
		$model->unsetAttributes();

		if (isset($_GET['Projectdetail']))
			$model->setAttributes($_GET['Projectdetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}