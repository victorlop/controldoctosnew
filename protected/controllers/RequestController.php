<?php

class RequestController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','child','documents','lastentries'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Request')));
	}

	public function actionCreate() {
		$model = new Request;
                $this->performAjaxValidation($model);
                
		if (isset($_POST['Request'])) {
			$model->setAttributes($_POST['Request']);
                        $model->active=1;
                        
			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}
                else
                {
                    $model->statusid = (int)DocumentsFlowOperation::RequestInitialFlowStatus();
                    $model->flowdoctoid =(int)DocumentsFlowOperation::RequestDefaultFlow ()->parametervalue;
                }

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Request');
                $this->performAjaxValidation($model);
    
		if (isset($_POST['Request'])) {
			$model->setAttributes($_POST['Request']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Request');

            if (!$row->status->allowinactivation)
                throw new CHttpException(404, 'Estado invalido para operacion.');
                
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Request('search');
		$model->unsetAttributes();

		if (isset($_GET['Request']))
			$model->setAttributes($_GET['Request']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Request('search');
		$model->unsetAttributes();

		if (isset($_GET['Request']))
			$model->setAttributes($_GET['Request']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionChild($id) {
                 $child_model = new Requestdetail("searchByParent");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('child',array(
                                    'model'=>$this->loadModel($id,'Request'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
        
        public function actiondocuments($id) {
                 $child_model = new Document("searchByRequest");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('documents',array(
                                    'model'=>$this->loadModel($id,'Request'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
        public function actionLastentries() {
            $model = new Request('lastentries');
            $model->unsetAttributes();

            if (isset($_GET['Request']))
                    $model->setAttributes($_GET['Request']);

            $this->render('lastentries', array(
                    'model' => $model,
            ));
        }       
}