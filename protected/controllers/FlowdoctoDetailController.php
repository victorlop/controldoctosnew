<?php

class FlowdoctoDetailController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'FlowdoctoDetail'),
		));
	}

	public function actionCreate($id) {
            
		$model = new FlowdoctoDetail;
                $model->flowid= $id;
                
                $this->performAjaxValidation($model);                
		if (isset($_POST['FlowdoctoDetail'])) {
			$model->setAttributes($_POST['FlowdoctoDetail']);

                        $sort = Yii::app()->db->createCommand()
                            ->select('max(sort) as maxSort')
                            ->from('flowdocto_detail')
                            ->queryScalar();
                        
                        $model->sort =0;

                        if ($sort)
                            $model->sort= $sort+1;
                        
			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'FlowdoctoDetail');


		if (isset($_POST['FlowdoctoDetail'])) {
			$model->setAttributes($_POST['FlowdoctoDetail']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'FlowdoctoDetail')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('FlowdoctoDetail');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new FlowdoctoDetail('search');
		$model->unsetAttributes();

		if (isset($_GET['FlowdoctoDetail']))
			$model->setAttributes($_GET['FlowdoctoDetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}