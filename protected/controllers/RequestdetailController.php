<?php

class RequestdetailController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Requestdetail')));
	}

	public function actionCreate($id) {
		$model = new Requestdetail;
                $model->requestid = $id;
                
                $this->performAjaxValidation($model);
                
                if (DocumentsFlowOperation::CanModifyRequest(Request::model()->findByPk($id)))
                {
                    EQuickDlgs::render('/site/error',array('code'=>654,'message'=>'Estado invalido para operacion'));
                    Yii::app()->end();
                    //throw new CHttpException(404, 'Estado invalido para operacion.');
                }
                else if (!DocumentsFlowOperation::CanModifyRequestMaxTopic(Request::model()->findByPk($id)))
                {
                    //throw new CHttpException(404, 'Máximo de Temas ha sido alcanzado.');
                    EQuickDlgs::render('/site/error',array('code'=>654,'message'=>'Máximo de Temas ha sido alcanzado.'));
                    Yii::app()->end();
                }

		if (isset($_POST['Requestdetail'])) {
			$model->setAttributes($_POST['Requestdetail']);
                        $Noerr = TRUE;
                        
                        if ($model->topicnew)
                        {
                            $topic = new Topic;
                            $topic->topicdescription = $model->topicnew;
                            $topic->topicused = 1;
                            $err=$topic->save();
                            var_dump($err);
                            
                            $model->topicnew=null;
                            $model->topicid = $topic->id;
                        }
                        else
                        {
                            $model->topic->topicused=1;
                            $model->topic->save();
                        }

                        if ($Noerr)
                        {
                            if ($model->save()) {
                                EQuickDlgs::checkDialogJsScript();
                                $this->redirect(array('admin'));
                            }
                        }
		}
		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Requestdetail');
                $this->performAjaxValidation($model);

		if (isset($_POST['Requestdetail'])) {
			$model->setAttributes($_POST['Requestdetail']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $requestDetail = $this->loadModel($id, 'Requestdetail');
            
            if (DocumentsFlowOperation::CanModifyRequest($requestDetail->request))
                throw new CHttpException(404, 'Estado invalido para operacion.');
            
            $topic = Topic::model()->findByPk ((int)$requestDetail->topicid);
            $topic->topicused = 0;
            $topic->save();
            
            $requestDetail->delete();

            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Requestdetail('search');
		$model->unsetAttributes();

		if (isset($_GET['Requestdetail']))
			$model->setAttributes($_GET['Requestdetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Requestdetail('search');
		$model->unsetAttributes();

		if (isset($_GET['Requestdetail']))
			$model->setAttributes($_GET['Requestdetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}