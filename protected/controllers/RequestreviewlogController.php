<?php

class RequestreviewlogController extends GxController {

        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionView($id) {
                EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Requestreviewlog')));
	}

	public function actionCreate($id) {
		$model = new Requestreviewlog;
                $model->requestid=$id;
                
                
                $model->status = Request::model()->findByPk($id)->statusid;

                $this->performAjaxValidation($model);
		if (isset($_POST['Requestreviewlog'])) {
			$model->setAttributes($_POST['Requestreviewlog']);
                        $fileName=null;
                        
                        $uploadedFile = CUploadedFile::getInstance($model, 'filename');  
                        if ($uploadedFile)
                        {
                            $fileName = "{$uploadedFile}"; // random number + file name
                            $uploadPath = Yii::app()->params['uploadPath'];
                        }

			if ($model->save()) {
                            
                            if ($uploadedFile)
                            {
                                $docto = new Document;
                                $docto->requestreviewlogid = $model->id;
                                $docto->filename = $fileName;
                                $docto->documentdescription = (!$model->documentname?$fileName:$model->documentname);
                                $docto->documentname = (!$model->documentname?$fileName:$model->documentname);
                                
                                if ($docto->save())
                                {
                                    $newPath =$uploadPath. '/request';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/request/'.$model->requestid;
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $newPath =$uploadPath. '/request/'.$model->requestid.'/requestreviewlog';
                                    if (!file_exists($newPath))
                                        mkdir($newPath);

                                    $uploadedFile->saveAs($newPath.'/['.$docto->id.']'.$fileName); // image will uplode to rootDirectory/uploads/fiels/    
                                }
                            }                            
                            
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Requestreviewlog');
                $this->performAjaxValidation($model);    
                
                if (isset($_POST['Requestreviewlog'])) {
			$model->setAttributes($_POST['Requestreviewlog']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}
                else {
                    $docto = Document::model()->find('requestreviewlogid=:requestviewlogid',array(':requestviewlogid'=>$id));
                    if ($docto)
                    {
                        $model->filename = $docto->filename;
                        $model->documentname =$docto->id;
                    }
                }

		EQuickDlgs::render('update',array('model'=>$model));	
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Requestreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Requestreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin($id) {
                $pend = PendingsProcess::model()->findByPk($id);
                //var_dump($pend);
                
                //$id = $id / ($pend->TipoId * 1000000);
                $id = ($id - $pend->TipoId) / ($pend->TipoId * 1000000);   
                
		$model = new Requestreviewlog('searchByParent');
		$model->unsetAttributes();
                $model->requestid = $id;

		if (isset($_GET['Requestreviewlog']))
			$model->setAttributes($_GET['Requestreviewlog']);

		$this->render('admin', array(
			'model' => $model,
                        'parentId' => $id
		));
	}

}