<?php

class PendingsReviewsController extends GxController {

    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'PendingsReviews'),
		));
	}

	public function actionCreate() {
		$model = new PendingsReviews;


		if (isset($_POST['PendingsReviews'])) {
			$model->setAttributes($_POST['PendingsReviews']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'PendingsReviews');
                $this->performAjaxValidation($model);                
                $id = ($id - $model->TipoId) / ($model->TipoId * 1000000);   

		if (isset($_POST['PendingsReviews'])) {
			$model->setAttributes($_POST['PendingsReviews']);

                        $transaction = $model->dbConnection->beginTransaction();
                        try
                        {
                            $st=  Status::model()->findByPk((int)$model->statusid);
                            
                            switch ($model->TipoId)
                            {
                                case 2:
                                    
                                    if ((int)$model->ended==1)
                                    {
                                        // verifico que hay ingresado al menos un comentario
                                        $cuantos = Preprojectreviewerlog::model()->count('preprojectreviewerid=:preprojectreviewerid',
                                                                                        array(':preprojectreviewerid'=>$id));
                                        if ($cuantos==0)
                                        {
                                            $model->ended = 0;
                                            $model->id = $id * ($model->TipoId * 1000000)+$model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'No se ha hecho ningun comentario.'));
                                        }
                                    }
                                    
                                    
                                    $preprj = Preprojectreviewer::model()->findByPk($id);
                                    $preprj->ended = $model->ended;
                                    $preprj->approved = $model->approved; 
                                    $preprj->dateend = new CDbExpression('NOW()');
                                
                                    $preprj->save();
                                break;
                                case 3:
                                    if ((int)$model->ended==1)
                                    {
                                        // verifico que hay ingresado al menos un comentario
                                        $cuantos = Projectreviewlog::model()->count('projectreviewid=:projectreviewid',
                                                                                        array(':projectreviewid'=>$id));
                                        if ($cuantos==0)
                                        {
                                            $model->ended = 0;
                                            $model->id = $id * ($model->TipoId * 1000000)+$model->TipoId;
                                            throw new CHttpException(400, Yii::t('app', 'No se ha hecho ningun comentario.'));
                                        }
                                    }
                                    
                                    
                                    $prj = Projectreview::model()->findByPk($id);
                                    $prj->ended = $model->ended;
                                    $prj->approved = $model->approved; 
                                    $prj->dateend = new CDbExpression('NOW()');
                                
                                    $prj->save();
                                break;
                            }
                            $transaction->commit();
                            
                            $model->id = ($model->id-$model->TipoId) / ($model->TipoId * 1000000);
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
                        }
                        
                        catch (Exception $e)
                        {
                            $transaction->rollBack();
                            switch ($model->TipoId)
                            {
                                case 2:
                                    $child_model = new Preprojectreviewerlog("searchByParent");
                                    break;
                                case 3:
                                    $child_model = new Projectreviewlog("searchByParent");
                                    break;
                            }
                            $child_model->unsetAttributes();
                            $model->id = ($model->id-$model->TipoId) / ($model->TipoId * 1000000);
                            
                            Yii::app()->user->setFlash('error',$e->getMessage());
                            /*EQuickDlgs::render('update',array('model'=>$model,
                                                                'child_model'=>$child_model,
                                                                'parentId' => $model->id));
                            Yii::app()->end();*/
                        }
		}

		switch ($model->TipoId)
                {
                    case 2:
                        $child_model = new Preprojectreviewerlog("searchByParent");
                        break;
                    case 3:
                        $child_model = new Projectreviewlog("searchByParent");
                        break;
                }
                $child_model->unsetAttributes();
                
                EQuickDlgs::render('update',array('model'=>$model,
                                                  'child_model'=>$child_model,
                                                  'parentId' => $id));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PendingsReviews')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new PendingsReviews('search');
		$model->unsetAttributes();

		if (isset($_GET['PendingsReviews']))
			$model->setAttributes($_GET['PendingsReviews']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new PendingsReviews('search');
		$model->unsetAttributes();

		if (isset($_GET['PendingsReviews']))
			$model->setAttributes($_GET['PendingsReviews']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}