<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preprojectreviewer-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
                <?php echo $form->hiddenField($model, 'preprojectid'); ?>
                <?php echo $form->hiddenField($model, 'statusid'); ?>
        
                <div class="row">
		<?php echo $form->labelEx($model,'teacherid'); ?>
		<?php 
                
                    $preprj= Preproject::model()->findByPk((int)$model->preprojectid);
                    $criteria = new CDbCriteria();
                    $criteria->select=array('t.teacherid');
                    if ($preprj->minreviewerid>0)
                    {
                        $minRev =(int)$preprj->minreviewerid;
                        if((int)$preprj->firstreview==0)
                            $minRev = $minRev + 1;
                        
                        $criteria->condition= 'preprojectid=:preprojectid AND statusid=:statusid and id>=:minrev';
                        $criteria->params=array(':preprojectid'=>$model->preprojectid,':statusid'=>$model->statusid,':minrev'=>$minRev);
                    }
                    else
                    {
                        $criteria->condition= 'preprojectid=:preprojectid AND statusid=:statusid';
                        $criteria->params=array(':preprojectid'=>$model->preprojectid,':statusid'=>$model->statusid);
                    }
                    
                    
                    $teachers= Preprojectreviewer::model()->findAll($criteria);
                    $teacherInc=array();
                    foreach ($teachers as $tch) {
                            $teacherInc[] = $tch->teacherid;
                     }
                
                     $criteria = new CDbCriteria;
                     $criteria->select = array('id,teachername, canreview, status');
                     $criteria->addNotInCondition('id',$teacherInc,TRUE);
                     
                     $teachers = Teacher::model()->findAll($criteria);
                     $tchAc=array();
                     foreach ($teachers as $tch)
                     {
                         if ((int)$tch->canreview == 1 &&
                              (int)$tch->status == 1    )
                                 $tchAc[$tch->id] = $tch->teachername;
                     }
                
                echo $form->dropDownList($model, 'teacherid', $tchAc,array('style'=>'width:60%;')); ?>
		<?php echo $form->error($model,'teacherid'); ?>
		</div><!-- row -->
                <?php 
                if (!$model->isNewRecord)
                { ?>
		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateend'); ?>
		<?php echo $form->textField($model, 'dateend'); ?>
		<?php echo $form->error($model,'dateend'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ended'); ?>
		<?php echo $form->textField($model, 'ended'); ?>
		<?php echo $form->error($model,'ended'); ?>
		</div><!-- row -->
                <?php } ?>

                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->