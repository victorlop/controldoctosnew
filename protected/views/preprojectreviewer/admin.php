<h1><?php echo Yii::t('app', 'Revisores Ante-proyecto'); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model_parent,
        'attributes' => array(
                array('label'=>'Descripción',
                    'value'=> $model_parent->preprojectdescription,),
                array('label' => 'Estudiante', 
                    'value'=> $model_parent->student->studentbatchid . ' - ' . $model_parent->student->studentname 
                ,),
            array('label' => 'Estado', 
                    'value'=> $model_parent->status->statusdescription
                ,),
                
        ),
)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'preprojectreviewer-grid',
	'dataProvider' => $model->searchByParent($parentId),
     'enablePagination' => false,
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'30'),),
                array(
                    'name'=>'teacherid',
                    'value'=>'GxHtml::valueEx($data->teacher)',
                    'filter'=>GxHtml::listDataEx(Teacher::model()->findAllAttributes(null, true)),
                    'htmlOptions'=>array('width'=>'120'),
                    ),
		array('name'=>'dateentry',
                      'htmlOptions'=>array('width'=>'75')
                    ),
		//'ended',
            array(
                       'name'=>'ended',
                       //'header'=>'Es final',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->ended=="1")?("Si"):("No")',
                       'htmlOptions'=>array('width'=>'60')
                    ),
            array('name'=>'dateend',
                      'htmlOptions'=>array('width'=>'75')
                    ),
            array(
                       'name'=>'approved',
                       //'header'=>'Es final',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->approved=="1")?("Si"):("No")',
                       'htmlOptions'=>array('width'=>'60')
                    ),
		
		/*array(
				'name'=>'preprojectid',
				'value'=>'GxHtml::valueEx($data->preproject)',
				'filter'=>GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true)),
				),*/
		/*
		array(
				'name'=>'statusid',
				'value'=>'GxHtml::valueEx($data->status)',
				'filter'=>GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'EJuiDlgsColumn',
                        'template'=>'{delete}{review}',
                        'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                        'buttons'=>array(
                            'delete' => array(
                                   'label'=> 'Eliminar',
                                    'visible'=> '!$data->ended',
                                ),
                            'review' => array(
                                    'label' => 'Detalle',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-chat2.png',
                                    'url'=>'Yii::app()->createUrl("preprojectreviewerlog/admin", array("id"=>$data->primaryKey+((int)Yii::app()->user->GetState("id")*2000000)))',        
                            ),

                        ),
		),
	),
)); ?>

<div class="row buttons">
<?php 
if (DocumentsFlowOperation::CanModifyPreProjectMaxReviewers($model_parent))
{
    EQuickDlgs::iframeButton(
        array(
            'controllerRoute' => 'preprojectreviewer/create/'.$parentId,
            'dialogTitle' => 'Agregar - Revisor',
            'dialogWidth' => 600,
            'dialogHeight' => 500,
            'hideTitleBar' => true,         
            'openButtonText' => 'Agregar',
            'closeButtonText' => 'Cerrar',
            'closeOnAction' => true, //important to invoke the close action in the actionCreate
            'refreshGridId' => 'preprojectreviewer-grid', //the grid with this id will be refreshed after closing
            'openButtonHtmlOptions' => array('class'=>'btn-grey')
        )
    );
}
?>
    <?php    echo GxHtml::button('Regresar', array('submit'=>array('pendingsProcess/index'),'class'=>'btn-grey')); ?>
</div>