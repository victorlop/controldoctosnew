<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'dateentry',
'dateend',
'ended',
array(
			'name' => 'teacher',
			'type' => 'raw',
			'value' => $model->teacher !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->teacher)), array('teacher/view', 'id' => GxActiveRecord::extractPkValue($model->teacher, true))) : null,
			),
array(
			'name' => 'preproject',
			'type' => 'raw',
			'value' => $model->preproject !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->preproject)), array('preproject/view', 'id' => GxActiveRecord::extractPkValue($model->preproject, true))) : null,
			),
array(
			'name' => 'status',
			'type' => 'raw',
			'value' => $model->status !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->status)), array('status/view', 'id' => GxActiveRecord::extractPkValue($model->status, true))) : null,
			),
	),
)); ?>

