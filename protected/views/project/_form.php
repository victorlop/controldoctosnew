<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'project-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
                <?php echo $form->hiddenField($model,'active'); 
                    echo $form->hiddenField($model,'active'); 
                ?>
                <div class="row">
		<?php echo $form->labelEx($model,'studentId'); ?>
		<?php echo $form->dropDownList($model, 'studentId', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1), array('prompt' => Yii::t('app', 'Todos')))),array('style'=>'width: 35%','disabled'=>TRUE)); ?>
		<?php echo $form->error($model,'studentId'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'projectdescription'); ?>
		<?php echo $form->textField($model, 'projectdescription', array('maxlength' => 1024,'size'=>50,'disabled'=> TRUE)); ?>
		<?php echo $form->error($model,'projectdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                        array('cols'=>50,
                                                'rows'=>5,
                                                'disabled'=>DocumentsFlowOperation::CanModifyProject($model))); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<?php
                if (!$model->isNewRecord)
                { ?>
                    <div class="row">
                    <?php echo $form->labelEx($model,'dateentry'); ?>
                    <?php echo $form->textField($model, 'dateentry',array('disabled'=>TRUE,'style'=>'width: 30%')); ?>
                    <?php echo $form->error($model,'dateentry'); ?>
                    </div><!-- row -->
                <?php 
                }
                else
                    echo $form->hiddenField($model,'dateentry'); 
                ?>

                <div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
		<?php 
                    if ($model->isNewRecord)
                    {
                        /*echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All')));*/ 
                        echo $form->hiddenField($model,'statusId');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)')));
                        //echo $form->hiddenField($model,'flowdoctoid',array('value'=>$valpar));
                    }
                    else
                    {
                        $dis = DocumentsFlowOperation::CanModifyProject($model);
                        
                        echo $form->hiddenField($model,'statusId');
                        //echo $form->hiddenField($model,'flowdoctoid');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)'),'disabled'=>$dis));
                    }
                ?>
                <?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->
		<label><?php //echo GxHtml::encode($model->getRelationLabel('projectdetails')); ?></label>
		<?php //echo $form->checkBoxList($model, 'projectdetails', GxHtml::encodeEx(GxHtml::listDataEx(Projectdetail::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php //echo GxHtml::encode($model->getRelationLabel('projectflowhistories')); ?></label>
		<?php //echo $form->checkBoxList($model, 'projectflowhistories', GxHtml::encodeEx(GxHtml::listDataEx(Projectflowhistory::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php //echo GxHtml::encode($model->getRelationLabel('projectreviews')); ?></label>
		<?php //echo $form->checkBoxList($model, 'projectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Projectreview::model()->findAllAttributes(null, true)), false, true)); ?>

<div class="row buttons">
                    <?php 
                        if (!DocumentsFlowOperation::CanModifyProject($model))
                            echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->