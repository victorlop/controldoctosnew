<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('projectdescription')); ?>:
	<?php echo GxHtml::encode($data->projectdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comments')); ?>:
	<?php echo GxHtml::encode($data->comments); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentId')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->student)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusId')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->status)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('preprojectid')); ?>:
	<?php echo GxHtml::encode($data->preprojectid); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('flowdoctoid')); ?>:
	<?php echo GxHtml::encode($data->flowdoctoid); ?>
	<br />
	*/ ?>

</div>