<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'projectdescription'); ?>
		<?php echo $form->textField($model, 'projectdescription', array('maxlength' => 1024,'size'=>50)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model, 'studentId'); ?>
		<?php echo $form->dropDownList($model, 'studentId', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model, 'statusId'); ?>
		<?php //echo $form->dropDownList($model, 'statusId', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

  	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
