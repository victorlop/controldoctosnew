<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('project-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Proyectos'); ?></h1>

<?php

$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'project-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
                    'name'=>'studentId',
                    'value'=>'GxHtml::valueEx($data->student)',
                    'htmlOptions'=>array('width'=>'150'),
                    'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
                    ),
		'projectdescription',
		//'comments',
		array('name'=>'dateentry','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		array(
                    'name'=>'statusId',
                    'filter'=>false,
                    'htmlOptions'=>array('width'=>'100'),
                    'value'=>'GxHtml::valueEx($data->status)',
                ),
		
		/*
		'preprojectid',
		'active',
		'flowdoctoid',
		*/
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{docum}',
                    'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                    'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                    'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                    'visible' => '!DocumentsFlowOperation::CanModifyProject($data)',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                   'visible'=>  '!DocumentsFlowOperation::CanModifyProject($data)',
                                ),
                                'detail' => array(
                                    'label' => 'Reportes',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    'url'=>'Yii::app()->createUrl("project/child", array("id"=>$data->primaryKey))',
                                ),
                                'docum' => array(
                                    'label' => 'Reportes avance',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-list.png',
                                    'url'=>'Yii::app()->createUrl("projectreporttask/admin", array("id"=>$data->primaryKey))',
                                ),
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Solicitud',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 500,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Solicitud',
                       'hideTitleBar' => true,                        
                       'dialogWidth' => 700, //override the value from the dialog config
                       'dialogHeight' => 600,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),

	),
)); ?>