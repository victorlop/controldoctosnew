
<h1><?php echo Yii::t('app', 'Proyectos de Investigación - Ingresos últimos 30 días'); ?></h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'project-grid',
	'dataProvider' => $model->lastentries(),
	//'filter' => $model,
	'columns' => array(
		//array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
                    'name'=>'studentId',
                    'value'=>'GxHtml::valueEx($data->student)',
                    'htmlOptions'=>array('width'=>'125'),
                    'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
                    ),
		'projectdescription',
		//'comments',
		array('name'=>'dateentry','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		array(
                    'name'=>'statusId',
                    'filter'=>false,
                    'htmlOptions'=>array('width'=>'150'),
                    'value'=>'GxHtml::valueEx($data->status)',
                ),
		
		/*
		'preprojectid',
		'active',
		'flowdoctoid',
		*/
		
	),
)); ?>