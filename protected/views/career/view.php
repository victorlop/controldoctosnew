<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<h1><?php echo Yii::t('app', 'Detalle')  ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
                                'id',
                                'code',            
                                'careerdescription',
                                'contactInfo',
                                'email',
                                'comments',
	),
)); ?>
