<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'career-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
		<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model, 'code', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'code'); ?>
		</div><!-- row -->
                
		<div class="row">
		<?php echo $form->labelEx($model,'careerdescription'); ?>
		<?php echo $form->textField($model, 'careerdescription', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'careerdescription'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'contactInfo'); ?>
		<?php echo $form->textArea($model, 'contactInfo',array('cols'=>60, 'rows'=>3)); ?>
		<?php echo $form->error($model,'contactInfo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('size'=>50,'maxlength' => 1024)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                            array('cols'=>60, 'rows'=>3)); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->