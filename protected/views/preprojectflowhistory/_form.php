<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preprojectflowhistory-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'entrydate'); ?>
		<?php echo $form->textField($model, 'entrydate'); ?>
		<?php echo $form->error($model,'entrydate'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectid'); ?>
		<?php echo $form->dropDownList($model, 'preprojectid', GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'preprojectid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusid'); ?>
		<?php echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'statusid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->dropDownList($model, 'userid', GxHtml::listDataEx(User::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'userid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->