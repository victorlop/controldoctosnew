<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('entrydate')); ?>:
	<?php echo GxHtml::encode($data->entrydate); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('preprojectid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->preproject)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->status)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('userid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />

</div>