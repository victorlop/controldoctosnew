<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'requestreviewlog-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>
        
	<?php //echo $form->errorSummary($model);
                echo $form->hiddenField($model, 'status');
                echo $form->hiddenField($model,'requestid');
                 ?>
		<div class="row">
                    <?php echo $form->labelEx($model,'comments'); ?>
                    <?php echo $form->textArea($model, 'comments',
                                            array('cols'=>60, 'rows'=>10,
                                                'disabled'=>!$model->isNewRecord));
                                                //'disabled'=>  !DocumentsFlowOperation::RequestGetEndStatus($model->requestid) && $model->isNewRecord)); ?>
                    <?php echo $form->error($model,'comments') ?>
		</div><!-- row -->
		<?php
                if ($model->isNewRecord)
                { ?>
                    <div class="row">
                        <?php echo $form->labelEx($model,'documentname'); ?>
                        <?php echo $form::textField($model,'documentname',array('size'=>60));  ?>
                        <?php echo $form->error($model,'documentname'); ?>
                    </div><!-- row --> 
                    <div class="row">
                        <?php echo $form->labelEx($model,'filename'); ?>
                        <?php echo CHtml::activeFileField($model,'filename');  ?>
                        <?php echo $form->error($model,'filename'); ?>
                    </div><!-- row -->       
                <?php
                } 
                else
                { ?>
                    <div class="row">
                        <?php echo $form->labelEx($model,'dateentry'); ?>
                        <?php echo $form->textField($model, 'dateentry',array('disabled'=>TRUE,'style'=>'width: 30%')); ?>
                        <?php echo $form->error($model,'dateentry'); ?>
                    </div><!-- row -->
                    <div class="row">
                        <?php 
                            if ($model->documentname) {
                                echo CHtml::label('Ver','im1' );
                        
                                $uploadPath = Yii::app()->params['uploadUrl'];
                                $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');
                                $newPath =Yii::app()->baseUrl.$uploadPath. '/request/'.$model->requestid.'/requestreviewlog/['.$model->documentname.']'.$model->filename;

                                echo CHtml::link($image,$newPath , array('target'=>'_blank','id=>im1')); 
                            }
               }                
               ?>
                    </div>
                <?php if (DocumentsFlowOperation::RequestGetEndStatus($model->requestid) &&
                            $model->isNewRecord) { ?>
                    
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
                <?php } ?>
    <?php $this->endWidget(); ?>
</div><!-- form -->