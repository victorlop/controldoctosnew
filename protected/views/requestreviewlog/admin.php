<?php


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requestreviewlog-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h1><?php echo Yii::t('app', 'Revisiones por Solicitud'); ?></h1>

<?php //echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div >
<?php //$this->renderPartial('_search', array('model' => $model,)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'requestreviewlog-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		'id',
		'dateentry',
		array('name'=>'comments','type'=>array('type'=>'shortText','length'=>50,'width'=>80,)),
		//'status',
		/*array(
				'name'=>'requestid',
				'value'=>'GxHtml::valueEx($data->request)',
				'filter'=>GxHtml::listDataEx(Request::model()->findAllAttributes(null, true)),
				),*/
		array(
			'class' => 'EJuiDlgsColumn',
                        'template'=>'{update}',
                        'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                        'buttons'=>array(
                            'update' => array(
                                    'label' => 'Ver',
                                    //'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    //'url'=>'Yii::app()->createUrl("pendingsProcess/update", array("id"=>$data->primaryKey))',        
                            ),
                            /*'review' => array(
                                    'label' => 'Procesar',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-chat2.png',
                                    'url'=>'Yii::app()->createUrl(($data->TipoId==1? "requestreviewlog" : ($data->TipoId==2? "preprojectreviewlog" :"projectreviewlog"))."/admin", array("id"=>$data->primaryKey))',        
                            )*/
                        ),
                    
                    'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Flujo de Trabajo',
                        'hideTitleBar' => TRUE,                        
                       'dialogWidth' => 700, //override the value from the dialog config
                       'dialogHeight' => 600,
                       'closeButtonText' => 'Cerrar'
                   ), 
		),
	),
)); ?>
<div class="row buttons">
<?php 
if (DocumentsFlowOperation::RequestGetEndStatus($model->requestid))
{
    EQuickDlgs::iframeButton(
        array(
            'controllerRoute' => 'requestreviewlog/create/'.$parentId,
            'dialogTitle' => 'Agregar',
            'dialogWidth' => 700,
            'dialogHeight' => 600,
            'hideTitleBar' => true,
            'openButtonText' => 'Agregar',
            'closeButtonText' => 'Cerrar',
            'closeOnAction' => true, //important to invoke the close action in the actionCreate
            'refreshGridId' => 'requestreviewlog-grid', //the grid with this id will be refreshed after closing
            'openButtonHtmlOptions' => array('class'=>'btn-grey')
        )
    );
}
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('pendingsProcess/index'),'class'=>'btn-grey')); ?>
</div>