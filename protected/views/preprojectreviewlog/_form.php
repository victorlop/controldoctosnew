<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preprojectreviewlog-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments'); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		<?php echo $form->textArea($model, 'filename'); ?>
		<?php echo $form->error($model,'filename'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'documentname'); ?>
		<?php echo $form->textArea($model, 'documentname'); ?>
		<?php echo $form->error($model,'documentname'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectid'); ?>
		<?php echo $form->dropDownList($model, 'preprojectid', GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'preprojectid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->