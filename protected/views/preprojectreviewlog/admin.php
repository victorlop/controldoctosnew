<?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model_parent,
        'attributes' => array(
                array('label'=>'Descripción',
                    'value'=> $model_parent->preprojectdescription,),
                array('label' => 'Estudiante', 
                    'value'=> $model_parent->student->studentbatchid . ' - ' . $model_parent->student->studentname 
                ,),
                
        ),
)); ?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'preprojectreviewlog-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'dateentry',
		'comments',
		'status',
		'filename',
		'documentname',
		/*
		array(
				'name'=>'preprojectid',
				'value'=>'GxHtml::valueEx($data->preproject)',
				'filter'=>GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>