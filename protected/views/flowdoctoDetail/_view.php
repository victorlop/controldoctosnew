<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('currentstatusid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->currentstatus)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('newstatusid')); ?>:
	<?php echo GxHtml::encode($data->newstatusid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('flowid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->flow)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notificationlistid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->notificationlist)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sort')); ?>:
	<?php echo GxHtml::encode($data->sort); ?>
	<br />

</div>