<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<p style="font-size: smaller">Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.</p>
	

	<div class="row">
		<?php echo $form->label($model, 'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024, 'size'=>50)); ?>
	</div>

        <div class="row">
		<?php echo $form->label($model, 'studentid'); ?>
		<?php echo $form->dropDownList($model, 'studentid', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))), array('prompt' => Yii::t('app', 'Todos'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                        array('cols'=>50,
                                                'rows'=>5)); ?>
	</div>

        <?php 
            $statusid = DocumentsFlowOperation::PreprojectInitialFlowStatus();
            

            /*echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All')));*/ 
            echo $form->hiddenField($model,'statusid',array('value'=>$statusid));

        ?>
	
	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
