<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('preprojectdescription')); ?>:
	<?php echo GxHtml::encode($data->preprojectdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comments')); ?>:
	<?php echo GxHtml::encode($data->comments); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentid')); ?>:
	<?php echo GxHtml::encode($data->studentid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusid')); ?>:
	<?php echo GxHtml::encode($data->statusid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('requestdetailid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->requestdetail)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('flowdoctoid')); ?>:
	<?php echo GxHtml::encode($data->flowdoctoid); ?>
	<br />
	*/ ?>

</div>