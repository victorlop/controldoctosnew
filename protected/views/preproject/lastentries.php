
<h1><?php echo Yii::t('app', 'Ante Proyectos de Investigación - Ingresos últimos 30 días'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'preproject-grid',
	'dataProvider' => $model->lastentries(),
	//'filter' => $model,
	'columns' => array(
		//array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
				'name'=>'studentid',
				'value'=>'GxHtml::valueEx($data->student)',
                                'htmlOptions'=>array('width'=>'125'),
				'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
				),
		'preprojectdescription',
		//'comments',
		array('name'=>'dateentry','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		
		array(
				'name'=>'statusid',
                                'filter'=>false,
                                'htmlOptions'=>array('width'=>'150'),
				'value'=>'GxHtml::valueEx($data->status)',
				),
		/*
		'active',
		array(
				'name'=>'requestdetailid',
				'value'=>'GxHtml::valueEx($data->requestdetail)',
				'filter'=>GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)),
				),
		'flowdoctoid',
		*/
		
	),
)); ?>