<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('preproject-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Ante Proyectos'); ?></h1>

<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'preproject-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
				'name'=>'studentid',
				'value'=>'GxHtml::valueEx($data->student)',
                                'htmlOptions'=>array('width'=>'150'),
				'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
				),
		'preprojectdescription',
		//'comments',
		array('name'=>'dateentry','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		
		array(
				'name'=>'statusid',
                                'filter'=>false,
                                'htmlOptions'=>array('width'=>'100'),
				'value'=>'GxHtml::valueEx($data->status)',
				),
		/*
		'active',
		array(
				'name'=>'requestdetailid',
				'value'=>'GxHtml::valueEx($data->requestdetail)',
				'filter'=>GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)),
				),
		'flowdoctoid',
		*/
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{docum}',
                    'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                    'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                    'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                    'visible' => 'DocumentsFlowOperation::PreProjectGetEndStatus($data->primaryKey)',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                   'visible'=>  '!DocumentsFlowOperation::CanModifyPreProject($data)',
                                ),
                                'detail' => array(
                                    'label' => 'Temas',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    'url'=>'Yii::app()->createUrl("preproject/child", array("id"=>$data->primaryKey))',
                                ),
                                'docum' => array(
                                    'label' => 'Documentos',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/attach.png',
                                    'url'=>'Yii::app()->createUrl("preproject/documents", array("id"=>$data->primaryKey))',
                                ),
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Solicitud',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 500,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Solicitud',
                       'hideTitleBar' => true,                        
                       'dialogWidth' => 700, //override the value from the dialog config
                       'dialogHeight' => 600,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>