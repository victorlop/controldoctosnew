<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preproject-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php 
                echo $form->hiddenField($model,'requestdetailid');
                echo $form->hiddenField($model,'active');
//echo $form->errorSummary($model); ?>
                <div class="row">
		<?php echo $form->labelEx($model,'studentid'); ?>
		<?php echo $form->dropDownList($model, 'studentid', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1), array('prompt' => Yii::t('app', 'Todos')))),array('style'=>'width: 35%','disabled'=>TRUE)); ?>
		<?php echo $form->error($model,'studentid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024,'size'=>50,'disabled'=> TRUE)); ?>
		<?php echo $form->error($model,'preprojectdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                        array('cols'=>50,
                                                'rows'=>5,
                                                'disabled'=>DocumentsFlowOperation::CanModifyPreProject($model))); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<?php
                if (!$model->isNewRecord)
                { ?>
                    <div class="row">
                    <?php echo $form->labelEx($model,'dateentry'); ?>
                    <?php echo $form->textField($model, 'dateentry',array('disabled'=>TRUE,'style'=>'width: 30%')); ?>
                    <?php echo $form->error($model,'dateentry'); ?>
                    </div><!-- row -->
                <?php 
                }
                else
                    echo $form->hiddenField($model,'dateentry'); 
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'documentsready'); ?>
                    <?php echo $form->checkBox($model,'documentsready',  array('disabled'=> DocumentsFlowOperation::CanModifyPreProject($model))); ?>
                    <?php echo $form->error($model,'documentsready'); ?>
                </div>
                <div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
		<?php 
                    if ($model->isNewRecord)
                    {
                        /*echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All')));*/ 
                        echo $form->hiddenField($model,'statusid');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)')));
                        //echo $form->hiddenField($model,'flowdoctoid',array('value'=>$valpar));
                    }
                    else
                    {
                        $dis = DocumentsFlowOperation::CanModifyPreProject($model);
                        
                        echo $form->hiddenField($model,'statusid');
                        //echo $form->hiddenField($model,'flowdoctoid');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)'),'disabled'=>$dis));
                    }
                ?>
                <?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->
                
		<label><?php //echo GxHtml::encode($model->getRelationLabel('preprojectreviewlogs')); ?></label>
		<?php //echo $form->checkBoxList($model, 'preprojectreviewlogs', GxHtml::encodeEx(GxHtml::listDataEx(Preprojectreviewlog::model()->findAllAttributes(null, true)), false, true)); ?>

<div class="row buttons">
                    <?php 
                        if (!DocumentsFlowOperation::CanModifyPreProject($model))
                            echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->