
<h1><?php echo Yii::t('app', 'Detalle')  ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'preprojectdescription',
            'student',
            'dateentry',
            'comments',
	),
)); ?>

