<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Actualizar'=> $this->renderPartial('_form', array('model' => $model,),true)),
    'options'=>array(
        'collapsible'=>FALSE,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>