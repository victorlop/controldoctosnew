<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('flowdocto-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Flujos de Trabajo'); ?></h1>
<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'flowdocto-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
		'flowdescription',
		'comments',
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{detail}',
                       'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                       'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                       'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',
                       'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                'detail' => array(
                                    'label' => 'Detalle',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    'url'=>'Yii::app()->createUrl("flowdocto/child", array("id"=>$data->primaryKey))',
                                )
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Flujo de Trabajo',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 450,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Flujo de Trabajo',
                        'hideTitleBar' => TRUE,                        
                       'dialogWidth' => 600, //override the value from the dialog config
                       'dialogHeight' => 450,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>

<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Flujo de Trabajo',
        'dialogWidth' => 600,
        'dialogHeight' => 450,
        'hideTitleBar' => TRUE,         
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'flowdocto-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey')
    )
);

?>