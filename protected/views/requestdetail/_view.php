<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('requestid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->request)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('topicid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->topic)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('approved')); ?>:
	<?php echo GxHtml::encode($data->approved); ?>
	<br />
        <?php echo GxHtml::encode($data->getAttributeLabel('topicnew')); ?>:
	<?php echo GxHtml::encode($data->topicnew); ?>
	<br />

</div>