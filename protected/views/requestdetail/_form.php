<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'requestdetail-form',
	'enableAjaxValidation' => TRUE,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<?php //echo $form->errorSummary($model); ?>
                <?php 
                    if ($model->isNewRecord)
                        echo $form->hiddenField($model,'approved', array('value'=>0)); 
                    else
                        echo $form->hiddenField($model,'approved');     
                    ?>
                <?php echo $form->hiddenField($model,'requestid'); ?>
		<div class="row">
                    <?php echo $form->labelEx($model,'topicid'); ?>
                    <?php 
                        $topics = Topic::model()->findAll('t.topicused=:used',array(':used'=>0));
                        $topicA=array();
                        foreach ($topics as $topic)
                            $topicA[$topic->id]=$topic->topicdescription;
                        
                        $this->widget('ext.widgets.select2.XSelect2', array(
                                'model'=>$model,
                                'attribute'=>'topicid',
                                'data'=>$topicA,
                                'htmlOptions'=>array('style'=>'width: 75%','prompt'=>'(seleccionar)'),
                                )                        
                        ) 
                    
                        //echo $form->dropDownList($model, 'topicid', GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true))); ?>
                    <?php echo $form->error($model,'topicid'); ?>
                </div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'topicnew'); ?>
                    <?php echo $form->textArea($model, 'topicnew', array('maxlength' => 1024,'style'=>'width:75%')); ?>
                    <?php echo $form->error($model,'topicnew'); ?>
                </div>
		<label><?php //echo GxHtml::encode($model->getRelationLabel('preprojects')); ?></label>
		<?php //echo $form->checkBoxList($model, 'preprojects', GxHtml::encodeEx(GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true)), false, true)); ?>

                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->