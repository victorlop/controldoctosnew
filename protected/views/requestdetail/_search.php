<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'requestid'); ?>
		<?php echo $form->dropDownList($model, 'requestid', GxHtml::listDataEx(Request::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'topicid'); ?>
		<?php echo $form->dropDownList($model, 'topicid', GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'approved'); ?>
		<?php echo $form->textField($model, 'approved'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model, 'topicnew'); ?>
		<?php echo $form->textField($model, 'topicnew', array('maxlength' => 1024)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
