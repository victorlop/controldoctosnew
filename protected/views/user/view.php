<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<h1><?php echo Yii::t('app', 'Detalle') ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
                'id',
                'username',
                'useridentity',
                'rol',
                /*array(
                    'name' => 'rol',
                    'type' => 'raw',
                    'value' => $model->rol !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rol)), array('rol/view', 'id' => GxActiveRecord::extractPkValue($model->rol, true))) : null,
                    ),*/
	),
)); ?>

