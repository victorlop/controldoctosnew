<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>
	<p class="note">
            <?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
            
	</p>
	<?php /*echo $form->errorSummary($model);*/ ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'password'); ?>
                    <?php echo $form->passwordField($model, 'password', array('size'=>40,'maxlength' => 40)); ?>
                    <?php echo $form->error($model,'password'); ?>
		</div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'passwordnew'); ?>
                    <?php echo $form->passwordField($model, 'passwordnew', array('size'=>40,'maxlength' => 40)); ?>
                    <?php /*$this->widget('ext.EStrongPassword.EStrongPassword', 
                            array('form'=>$form, 'model'=>$model, 'attribute'=>'passwordnew',
                                'requirementOptions'=>array('minChar'=>6,'one_special_char'=>true,
                                    'minCharText'=> 'Debe tener al menos %d carácteres',
                                    'displayMinChar'=> FALSE)))*/ ?>
                    <?php echo $form->error($model,'passwordnew'); ?>
		</div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'passwordconf'); ?>
                    <?php echo $form->passwordField($model, 'passwordconf', array('size'=>40,'maxlength' => 40)); ?>
                    <?php echo $form->error($model,'passwordconf'); ?>
		</div><!-- row -->
		
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', 'Cambiar')); ?>
                </div>
    <?php $this->endWidget(); ?>
                
                
<?php if(Yii::app()->user->hasFlash('changepasssuccess')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('changepasssuccess'); ?>
</div>

<?php elseif (Yii::app()->user->hasFlash('changepasserror')): ?>
    <div class="flash-error">
	<?php echo Yii::app()->user->getFlash('changepasserror'); ?>
    </div>
    <div class="row">
                <?php //echo CHtml::link('Intentar nuevamente',Yii::app()->createUrl("site/requestpassword")); ?>
    </div>
                <?php endif; ?>
</div><!-- form -->