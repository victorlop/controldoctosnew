<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<p style="font-size: smaller">Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.</p>
	<div class="row">
		<?php echo $form->label($model, 'username'); ?>
		<?php echo $form->textField($model, 'username', array('maxlength' => 256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'useridentity'); ?>
		<?php echo $form->textField($model, 'useridentity', array('maxlength' => 1024, 'size'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'rolid'); ?>
		<?php 
                    echo $form->dropDownList($model, 'rolid', GxHtml::listDataEx(Rol::model()->findAllAttributes(null, true,'active=:active',array(':active'=>1))), array('prompt' => Yii::t('app', 'Todos'))); 
                
                    /*$roles = Rol::model()->findAll('t.active=:active',array(':active'=>1));
                    $rolA=array();
                    foreach ($roles as $rol)
                        $rolA[$rol->id]=$rol->roldescription;
                    $this->widget('ext.widgets.select2.XSelect2', array(
                            'model'=>$model,
                            'attribute'=>'rolid',
                            'data'=>$rolA,
                            'htmlOptions'=>array('prompt'=>'Todos','style'=>'width: 30%'),
                            )                        
                    ) */               
                ?>
            
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
