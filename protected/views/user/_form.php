<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>
	<p class="note">
            <?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>
	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php echo $form->hiddenField($model,'status'); ?>
		<div class="row">
                    <?php echo $form->labelEx($model,'username'); ?>
                    <?php echo $form->textField($model,'username',array('maxlength'=>256,'disabled'=>(!$model->isNewRecord))); ?>
                    <?php echo $form->error($model,'username'); ?>
		</div><!-- row -->
		<div class="row">
                    <?php echo $form->labelEx($model,'useridentity'); ?>
                    <?php echo $form->textField($model, 'useridentity', array('size'=>50,'maxlength' => 1024)); ?>
                    <?php echo $form->error($model,'useridentity'); ?>
		</div><!-- row -->
                <div class="row">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->textField($model, 'email', array('size'=>50,'maxlength' => 1024)); ?>
                    <?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
		<div class="row">
                    <?php echo $form->labelEx($model,'rolid'); ?>
                    <?php 
                        $active=1;
                        echo $form->dropDownList($model, 'rolid', GxHtml::listDataEx(Rol::model()->findAllAttributes(null,true,'active=:active',array(':active'=>$active)))); ?>
                    <?php echo $form->error($model,'rolid'); ?>
		</div><!-- row -->
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->