<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Agregar'=> $this->renderPartial('_form', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>false,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>
<?php if (Yii::app()->user->hasFlash('usererror')): ?>
    <div class="flash-error">
	<?php echo Yii::app()->user->getFlash('usererror'); ?>
    </div>
<?php endif; ?>