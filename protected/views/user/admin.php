<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});


");
?>
<h1><?php echo Yii::t('app', 'Usuarios'); ?></h1>

<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'user-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
                array('name'=>'username','htmlOptions'=>array('width'=>'120'),),
                array('name'=>'useridentity'),
		array(
				'name'=>'rolid',
				'value'=>'GxHtml::valueEx($data->rol)',
				/*'filter'=>$this->widget('ext.select2.ESelect2',array(
                                                                                    'name'=>'rolid',
                                                                                    'data'=>Rol::model()->findAllAttributes(null, true,'active=:active',array(':active'=>1)),
                                                                                )            
                                            ),
                    
                                    //GxHtml::listDataEx(Rol::model()->findAllAttributes(null, true,'active=:active',array(':active'=>1), array('prompt' => Yii::t('app', 'Todos')))),
                                  */
                                 
				),
                array(
                   'class'=>'EJuiDlgsColumn',
                       'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                       'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                       'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),

                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                
                    ),
                    'viewDialog'=>array(
                        'controllerRoute' => 'view', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Detalle - Usuario',
                        'hideTitleBar' => true, 
                        'dialogWidth' => 600, //use the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar',
                        
                    ),
                    'updateDialog'=>array(
                        'controllerRoute' => 'update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Modificar - Usuario',
                        'hideTitleBar' => TRUE, 
                        'dialogWidth' => 700, //override the value from the dialog config
                        'dialogHeight' => 600,
                        'closeButtonText' => 'Cerrar'
                    ),            
               ),
	),
)); ?>
<div class="row buttons">
<?php 
    EQuickDlgs::iframeButton(
        array(
            'controllerRoute' => 'create',
            'dialogTitle' => 'Agregar - Usuario',
            'dialogWidth' => 700,
            'dialogHeight' => 600,
            'hideTitleBar' => true,
            'openButtonText' => 'Agregar',
            'closeButtonText' => 'Cerrar',
            'closeOnAction' => true, //important to invoke the close action in the actionCreate
            'refreshGridId' => 'user-grid', //the grid with this id will be refreshed after closing
            'openButtonHtmlOptions' => array('class'=>'btn-grey'),
        )
    );
    
?>
    
    
</div>

