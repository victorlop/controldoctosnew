<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'projectreview-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
            <?php echo $form->hiddenField($model, 'projectid'); ?>
                <?php echo $form->hiddenField($model, 'statusid'); ?>
                <div class="row">
		<?php echo $form->labelEx($model,'teacherid'); ?>
		<?php 
                    $prj= Project::model()->findByPk((int)$model->projectid);
                    $criteria = new CDbCriteria();
                    $criteria->select=array('t.teacherid');
                    if ($prj->minreviewerid>0)
                    {
                        $minRev =(int)$prj->minreviewerid;
                        if((int)$prj->firstreview==0)
                            $minRev = $minRev + 1;
                        
                        $criteria->condition= 'projectid=:projectid AND statusid=:statusid and id>=:minrev';
                        $criteria->params=array(':projectid'=>$model->projectid,':statusid'=>$model->statusid,':minrev'=>$minRev);
                    }
                    else
                    {
                        $criteria->condition= 'projectid=:projectid AND statusid=:statusid';
                        $criteria->params=array(':projectid'=>$model->projectid,':statusid'=>$model->statusid);
                    }
                    
                    
                    $teachers= Projectreview::model()->findAll($criteria);
                    $teacherInc=array();
                    foreach ($teachers as $tch) {
                            $teacherInc[] = $tch->teacherid;
                     }
                
                     $criteria = new CDbCriteria;
                     $criteria->select = array('id,teachername, canreview, status');
                     $criteria->addNotInCondition('id',$teacherInc,TRUE);
                     
                     $teachers = Teacher::model()->findAll($criteria);
                     $tchAc=array();
                     foreach ($teachers as $tch)
                     {
                         if ((int)$tch->canreview == 1 &&
                              (int)$tch->status == 1    )
                                 $tchAc[$tch->id] = $tch->teachername;
                     }
                
                echo $form->dropDownList($model, 'teacherid', $tchAc,array('style'=>'width:60%;'));
                    //echo $form->dropDownList($model, 'teacherid', GxHtml::listDataEx(Teacher::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'teacherid'); ?>
		</div><!-- row -->
                
		<?php 
                if (!$model->isNewRecord)
                { ?>
		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateend'); ?>
		<?php echo $form->textField($model, 'dateend'); ?>
		<?php echo $form->error($model,'dateend'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ended'); ?>
		<?php echo $form->textField($model, 'ended'); ?>
		<?php echo $form->error($model,'ended'); ?>
		</div><!-- row -->
                <?php } ?>

                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->