<h1><?php echo Yii::t('app', 'Revisores Proyecto'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model_parent,
        'attributes' => array(
                array('label'=>'Descripción',
                    'value'=> $model_parent->projectdescription,),
                array('label' => 'Estudiante', 
                    'value'=> $model_parent->student->studentbatchid . ' - ' . $model_parent->student->studentname 
                ,),
            array('label' => 'Estado', 
                    'value'=> $model_parent->status->statusdescription
                ,),
                
        ),
)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'projectreview-grid',
	'dataProvider' => $model->searchByParent($parentId),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'30'),),
                array(
                    'name'=>'teacherid',
                    'value'=>'GxHtml::valueEx($data->teacher)',
                    'filter'=>GxHtml::listDataEx(Teacher::model()->findAllAttributes(null, true)),
                    'htmlOptions'=>array('width'=>'120'),
                    ),
		array('name'=>'dateentry',
                      'htmlOptions'=>array('width'=>'75')
                    ),
		//'ended',
            array(
                       'name'=>'ended',
                       //'header'=>'Es final',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->ended=="1")?("Si"):("No")',
                       'htmlOptions'=>array('width'=>'60')
                    ),
            array('name'=>'dateend',
                      'htmlOptions'=>array('width'=>'75')
                    ),
            array(
                       'name'=>'approved',
                       //'header'=>'Es final',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->approved=="1")?("Si"):("No")',
                       'htmlOptions'=>array('width'=>'60')
                    ),
		array(
			'class' => 'EJuiDlgsColumn',
                        'template'=>'{delete}{review}',
                        'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                        'buttons'=>array(
                            'delete' => array(
                                   'label'=> 'Eliminar',
                                    'visible'=> '!$data->ended',
                                ),
                            'review' => array(
                                    'label' => 'Detalle',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-chat2.png',
                                    'url'=>'Yii::app()->createUrl("projectreviewlog/admin", array("id"=>$data->primaryKey * 3000000  + 3))',        
                            ),

                        ),
		),
	),
)); ?>

<div class="row buttons">
<?php 
if (DocumentsFlowOperation::CanModifyProjectMaxReviewers($model_parent))
{
    EQuickDlgs::iframeButton(
        array(
            'controllerRoute' => 'projectreview/create/'.$parentId,
            'dialogTitle' => 'Agregar - Revisor',
            'dialogWidth' => 600,
            'dialogHeight' => 500,
            'hideTitleBar' => true,         
            'openButtonText' => 'Agregar',
            'closeButtonText' => 'Cerrar',
            'closeOnAction' => true, //important to invoke the close action in the actionCreate
            'refreshGridId' => 'projectreview-grid', //the grid with this id will be refreshed after closing
            'openButtonHtmlOptions' => array('class'=>'btn-grey')
        )
    );
}
?>
    <?php    echo GxHtml::button('Regresar', array('submit'=>array('pendingsProcess/index'),'class'=>'btn-grey')); ?>
</div>