<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('courseid')); ?>:
	<?php echo GxHtml::encode($data->courseid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentid')); ?>:
	<?php echo GxHtml::encode($data->studentid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />

</div>