    <h1><?php echo Yii::t('app', 'Estados por rol'); ?></h1>

        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array('label' => 'Rol', 
                        'value'=> $model->roldescription
                        ,)
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByParent($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            array(
                                'name'=>'Opcion',
                                'value'=>'GxHtml::valueEx($data->status)',
                                'filter'=>GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)),
                            ),
                            array(
                                    'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                                
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("rolstatus/delete", array("id"=>$data->primaryKey))',
                                                 ),

                                    ),
                                ),
                            ),
                    ));
            
        ?>
<div class="row buttons">
<?php 
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'rolstatus/create/'.$parentId,
        'dialogTitle' => 'Agregar',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'hideTitleBar' => true,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'child-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey'),
    )
);
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('rol/admin'),'class'=>'btn-grey')); ?>
</div>
