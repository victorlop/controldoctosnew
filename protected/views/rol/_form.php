<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rol-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>
	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php echo $form->hiddenField($model,'active'); ?>
		<div class="row">
		<?php echo $form->labelEx($model,'roldescription'); ?>
		<?php echo $form->textField($model, 'roldescription', array('maxlength' => 50,'size'=>50)); ?>
		<?php echo $form->error($model,'roldescription'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'usedashboard'); ?>
		<?php echo $form->checkBox($model, 'usedashboard'); ?>
		<?php echo $form->error($model,'usedashboard'); ?>
		</div><!-- row -->
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('rolstatuses'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'rolstatuses', GxHtml::encodeEx(GxHtml::listDataEx(Rolstatus::model()->findAllAttributes(null, true)), false, true));*/ ?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('rolsystemoptions')); */?></label>
		<?php /*echo $form->checkBoxList($model, 'rolsystemoptions', GxHtml::encodeEx(GxHtml::listDataEx(Rolsystemoption::model()->findAllAttributes(null, true),'id','systemOption.description'), false, true)); */?>
                		<label><?php /*echo GxHtml::encode($model->getRelationLabel('users'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'users', GxHtml::encodeEx(GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), false, true)); */
        ?>
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
        


</div><!-- form -->