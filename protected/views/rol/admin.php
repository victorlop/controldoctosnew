<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rol-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Roles'); ?></h1>

<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'rol-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
		'roldescription',
		/*array(
			'class'=>'CButtonColumn',
		),*/
                array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{detail}{status}',
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                    'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                    'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',
                    
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                'detail' => array(
                                    'label' => 'Permisos',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/key.png',
                                    'url'=>'Yii::app()->createUrl("rol/child", array("id"=>$data->primaryKey))',
                                ),
                                'status'=>array(
                                    'label' => 'Estados donde participa',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    'url'=>'Yii::app()->createUrl("rol/status", array("id"=>$data->primaryKey))',
                                ),
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Rol',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 400,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Rol',
                       'hideTitleBar' => true,
                       'dialogWidth' => 600, //override the value from the dialog config
                       'dialogHeight' => 400,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>
<div class="row buttons">
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Rol',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'hideTitleBar' => true,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'rol-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey'),
    )
);

?>
</div>