<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<p style="font-size: smaller">Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.</p>
	<div class="row">
		<?php echo $form->label($model, 'roldescription'); ?>
		<?php echo $form->textField($model, 'roldescription', array('maxlength' => 50,'size'=>'50')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
