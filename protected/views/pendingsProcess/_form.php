<div class="wide form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'pendings-process-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>
        <?php 
        

            if ($model->id < 1000000)
                $model->id = $model->id * ($model->TipoId * 1000000) + $model->TipoId;
            
            if ($model->TipoId==1) //Solicitud
            {
                 $status= Status::model()->findAll('approve=1 and endstatus=1');
                 $staInc=array();
                 foreach ($status as $opc) {
                    $staInc[] = (int)$opc->id;
                 }

                 $apppar=  Appparameter::model()->find('code=:code',array(':code'=>'REQ'));
                 $flowid = (int)$apppar->parametervalue;
                
                 $criteria = new CDbCriteria;
                 $criteria->select = array('id,newstatusid');
                 $criteria->limit=1;
                 $criteria->condition = 'flowid=:flwid';
                 $criteria->params = array(':flwid'=>$flowid);
                 $criteria->addInCondition('newstatusid',$staInc);
                 
                $flow = FlowdoctoDetail::model()->find($criteria);
                
                if ($flow)
                {
                        echo '<input type="hidden" id="aprv" value="'.$flow->newstatusid.'" >';
                }
                
            }
        ?>
	<?php //echo $form->errorSummary($model); ?>
        <?php echo $form->hiddenField($model, 'TipoId'); ?>
        <?php echo $form->hiddenField($model, 'Tipo'); ?>
        <?php echo $form->hiddenField($model, 'studentid'); ?>
        <?php echo $form->hiddenField($model, 'statusdescription'); ?>
        <?php echo $form->hiddenField($model, 'flowdoctoid'); ?>
        <?php echo $form->hiddenField($model, 'id'); ?>
        <div class="row">
        <?php echo $form->labelEx($model,'id'); ?>
            <input id="id1" maxlength="21" disabled="true" 
                   value="<?php 
                                echo ($model->id - $model->TipoId) / ($model->TipoId * 1000000) ;
                            ?>" >
        <?php //echo $form->textField($model, 'id', array('maxlength' => 21,'disabled'=>TRUE)); ?>
        <?php echo $form->error($model,'id'); ?>
        </div><!-- row -->
        <div class="row">
        <?php echo $form->labelEx($model,'studentname'); ?>
        <?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50,'disabled'=>TRUE)); echo $form->textField($model, 'studentname', array('maxlength' => 256,'size'=>50,'disabled'=>TRUE));?>
        <?php //echo $form->error($model,'studentbatchid'); ?>
        </div><!-- row -->
        <div class="row">
        <?php //echo $form->labelEx($model,'studentname'); ?>
        <?php //echo $form->textField($model, 'studentname', array('maxlength' => 256,'size'=>50,'disabled'=>TRUE)); ?>
        <?php //echo $form->error($model,'studentname'); ?>
        </div><!-- row -->

        <div class="row">
        <?php echo $form->labelEx($model,'entrydate'); ?>
        <?php echo $form->textField($model, 'entrydate',array('disabled'=>TRUE,'size'=>25)); ?>
        <?php echo $form->error($model,'entrydate'); ?>
        </div><!-- row -->
        <div class="row">
        <?php echo $form->labelEx($model,'statusid'); ?>
        <?php 
                echo $form->dropDownList($model, 'statusid', DocumentsFlowOperation::NextFlowStatus($model->statusid, $model->flowdoctoid));
            //echo $form->textField($model, 'statusid', array('maxlength' => 20)); ?>
        <?php echo $form->error($model,'statusid'); ?>
        </div><!-- row -->
        <?php if ($model->TipoId==1) //Solicitud
            {?>
            <div id="row">
                <?php echo $form->labelEx($model,'topicid'); ?>
                <?php 
                    $varId = $model->id - $model->TipoId;                                
                    $idReq = $varId / ($model->TipoId * 1000000);

                    $reqDet=array();
                    $reqDets = Requestdetail::model()->findAll('requestid=:idR',array(':idR'=>$idReq));

                    foreach($reqDets as $itm) {       
                        $topc=Topic::model()->findByPk($itm->topicid);
                        if ($topc) $reqDet[(int)$topc->id]= $topc;
                    }

                    echo $form->dropDownList($model, 'topicid',$reqDet,array('empty'=>'(seleccionar)','style'=>'width:30%;')); 

                  ?>
                 <?php echo $form->error($model,'topicid'); ?>
            </div>
            <?php }
                if ($model->TipoId!=1)
                {
            ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'filename'); ?>
                    <?php echo CHtml::activeFileField($model,'filename');  ?>
                    <?php echo $form->error($model,'filename'); ?>
                </div><!-- row -->  
            <?php
                }
            ?>
        
        <div class="row buttons">
            <?php
            echo GxHtml::submitButton(Yii::t('app', 'Grabar'));
           
            $this->endWidget();
            ?>
        </div>
        
</div><!-- form -->
<script src="<?php echo Yii::app()->baseUrl.'/js/GralFunc.js' ?>" type="text/javascript"></script>