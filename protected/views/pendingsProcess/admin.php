<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('pendings-process-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<h1><?php echo Yii::t('app', 'Documentos en Proceso'); ?></h1>


<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>false,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'pendings-process-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		'id1' => array(
                    'name'=>'id',
                    'value'=> function($data)   {
                        return ($data->id - $data->TipoId) / ($data->TipoId * 1000000);
                    }
                ),
		'Tipo',
		//'studentid',
                'studentbatchid',
		'studentname',
		'entrydate',
		//'statusid',
                'statusdescription',
		/*
		'statusdescription',
		'flowdoctoid',
		*/
		array(
			'class' => 'EJuiDlgsColumn',
                        'template'=>'{update}{review}{reviewers}',
                        'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                        'buttons'=>array(
                            'update' => array(
                                    'label' => 'Procesar',
                                    'visible'=> '!$data->endstatus',
                                    //'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    //'url'=>'Yii::app()->createUrl("pendingsProcess/update", array("id"=>$data->primaryKey))',        
                            ),
                            'review' => array(
                                    'label' => 'Anotaciones',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-chat2.png',
                                    'url'=>'Yii::app()->createUrl(($data->TipoId==1? "requestreviewlog" : ($data->TipoId==2? "preprojectreviewlog" :"projectreviewlog"))."/admin", array("id"=>$data->primaryKey))',        
                                    'visible'=>'$data->TipoId==1? TRUE : FALSE',
                            ),
                            'reviewers' => array(
                                    'label' => 'Revisores',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/reviewers.png',
                                    'url'=>'Yii::app()->createUrl("/".($data->TipoId==2 ? "preprojectreviewer" : "projectreview")."/admin", array("id"=>$data->primaryKey))',        
                                    'visible'=> '($data->TipoId == 1 ? FALSE : ($data->TipoId==2 ? PreProjectOperation::ShowReviewers($data->statusid) : ProjectOperation::ShowReviewers($data->statusid)))',
                            )
                        ),
                    
                    'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Flujo de Trabajo',
                        'hideTitleBar' => TRUE,                        
                       'dialogWidth' => 900, //override the value from the dialog config
                       'dialogHeight' => 700,
                       'closeButtonText' => 'Cerrar'
                   ), 
		),
	),
)); ?>