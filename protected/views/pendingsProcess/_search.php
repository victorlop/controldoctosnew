<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'Tipo'); ?>
		<?php echo $form->dropDownList($model, 'Tipo', array('Solicitud'=>'Solicitud','Ante-Proyecto'=>'Ante-Proyecto','Proyecto'=>'Proyecto'),array('prompt'=>'Todos')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'studentid'); ?>
		<?php 
                    $students = Student::model()->findAll('status=:status',array(':status'=>1));
                    $studentA=array();
                    foreach ($students as $student)
                        $studentA[$student->id]=$student->studentbatchid . ' - ' . $student->studentname;

                    $this->widget('ext.widgets.select2.XSelect2', array(
                            'model'=>$model,
                            'attribute'=>'studentid',
                            'data'=>$studentA,
                            'htmlOptions'=>array('style'=>'width: 50%','prompt'=>'Todos'),
                            )                        
                    )
                                
                    //echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'statusid'); ?>
		<?php 
                    $criteria = new CDbCriteria();
                    $criteria->select=array('statusid');
                    $criteria->condition= 'rolid=:rolid ';
                    $criteria->params=array(':rolid'=>Yii::app()->user->GetState('rolid'));
                    
                    $opciones= Rolstatus::model()->findAll($criteria);
                    $opcInc=array();
                    foreach ($opciones as $opc) {
                        $opcInc[] = $opc->statusid;
                    }
                
                    $criteria = new CDbCriteria;
                    $criteria->select = array('id,description');
                    $criteria->addInCondition('id',$opcInc,TRUE);
                    
                    echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true,$criteria)), array('prompt' => Yii::t('app', 'Todos'))); ?>
                
	</div>
        <div class="row">
            <?php echo $form->label($model, 'endstatus'); ?>
            <?php echo $form->checkBox($model, 'endstatus'); ?>
        </div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Filtrar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
