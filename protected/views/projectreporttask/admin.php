<h1><?php echo Yii::t('app', 'Reporte de Avances'); ?></h1>
        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                        array('label'=>'Proyecto',
                            'value'=> $model->projectdescription,),
                        array('label' => 'Estudiante', 
                            'value'=> $model->student->studentbatchid . ' - ' . $model->student->studentname 
                        ,), 
                        array(
                          'label'  =>'Documento completo',
                            'value' => $model->completed ? 'Si' : 'No'
                        ,),
                    
                ),
        )); ?>

<?php echo '<input type="hidden" id="projectreporttask_completed" value="'.($model->completed?1:0).'" />' ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'projectreporttask-grid',
	'dataProvider' => $child_model->searchByParent($parentId),
	//'filter' => $model,
	'columns' => array(
		//'id',
		array('name'=>'dateentry','filter'=>false,
                    'value'=>'date("Y-m-d",strtotime($data->dateentry))',
                    'htmlOptions'=>array('width'=>'100'),),
                array('name'=>'reporttaskdescription','type'=>array('type'=>'shortText','length'=>75,'width'=>75,)),    
                array(
                    'name'=>'name',
                    'htmlOptions'=>array('width'=>1,'style' => 'text-align: right;'),
                    'type'=>'raw',
                    'header'=>'',
                    'value'=>function($data,$row) {
    
                                                $docto = Document::model()->find('projectreporttaskid=:id',array(':id'=>$data->primaryKey));
                                                if ($docto)
                                                {
                                                    $review= Projectreporttask::model()->findByPk($data->primaryKey);
                                                    $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');
                                                    $uploadPath = Yii::app()->params['uploadUrl']; 
                                                    $uploadPath = Yii::app()->baseUrl.$uploadPath .'/project/'.$data->projectid.'/Avances';
                                                    $uploadPath = $uploadPath.'/['.$data->primaryKey.']'.$docto->filename;

                                                    //return Yii::app()->createUrl($uploadPath);
                                                    return CHtml::link($image,$uploadPath , array('target'=>'_blank','title'=>$docto->documentdescription)); 
                                                }
                                                return null;
                                            },
                
            ),
		/*array(
				'name'=>'projectid',
				'value'=>'GxHtml::valueEx($data->project)',
				'filter'=>GxHtml::listDataEx(Project::model()->findAllAttributes(null, true)),
				),*/
		/*'completed',
                array(
                       'name'=>'completed',
                       'header'=>'Completado',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->endstatus=="1")?("Si"):("No")'
                    ),*/
		array(
                                     'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     //'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                                     'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',  
                                     //'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("projectreporttask/delete", array("id"=>$data->primaryKey))',
                                                     'visible'=>  '!DocumentsFlowOperation::CanModifyProjectReportTask(Project::model()->findByPk($data->projectid))',
                                                 ),
                                               
                                        ),
                                   'viewDialog'=>array(
                                        'controllerRoute' => 'view', //=default
                                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                                        'dialogTitle' => 'Detalle - Documento',
                                        //'hideTitleBar' => true, 
                                        'dialogWidth' => 600, //use the value from the dialog config
                                        'dialogHeight' => 400,
                                        'closeButtonText' => 'Cerrar'
                                    ),
                                    'updateDialog'=>array(
                                        'controllerRoute' => 'projectreporttask/update', //=default
                                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                                        'dialogTitle' => 'Modificar - Documento',
                                        'hideTitleBar' => true,                         
                                        'dialogWidth' => 700, //override the value from the dialog config
                                        'dialogHeight' => 550,
                                        'closeButtonText' => 'Cerrar'
                                    ),  
                                ),
                            ),
                    ));
            
?>
<div class="row buttons">    
<?php 

    if (!DocumentsFlowOperation::CanModifyProject($model) && DocumentsFlowOperation::ProjectGetEndStatus($model->id) )
    {
        EQuickDlgs::iframeButton(
            array(
                'controllerRoute' => 'projectreporttask/create/'.$parentId,
                'dialogTitle' => 'Agregar ',
                'dialogWidth' => 700,
                'dialogHeight' => 550,
                'openButtonText' => 'Agregar',
                'closeButtonText' => 'Cerrar',
                'hideTitleBar' => TRUE,
                'closeOnAction' => true, //important to invoke the close action in the actionCreate
                'refreshGridId' => 'projectreporttask-grid', //the grid with this id will be refreshed after closing
                'openButtonHtmlOptions' => array('class'=>'btn-grey','id'=>'btDis_projectreporttask_completed')
            )
        );
    }
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('project/admin'),'class'=>'btn-grey')); ?>
</div>
<script src="<?php echo Yii::app()->baseUrl.'/js/GralFunc.js' ?>" type="text/javascript"></script>