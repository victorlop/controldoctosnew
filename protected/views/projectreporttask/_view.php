<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('reporttaskdescription')); ?>:
	<?php echo GxHtml::encode($data->reporttaskdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('projectid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->project)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('completed')); ?>:
	<?php echo GxHtml::encode($data->completed); ?>
	<br />

</div>