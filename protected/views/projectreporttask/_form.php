<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'projectreporttask-form',
	'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); 
            $form->hiddenField($model,'projectid');
        ?>

        <div class="row">
        <?php echo $form->labelEx($model,'dateentry'); ?>
        <?php //echo $form->textField($model, 'dateentry',array('disabled'=>($model->isNewRecord? TRUE : FALSE ))); 
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',array(
                'language'=>'',
                'model'=>$model,                                // Model object
                'attribute'=>'dateentry', // Attribute name
                'mode'=>'date',                     // Use "time","date" or "datetime" (default)
                //'options'=>array(),                     // jquery plugin options
                'htmlOptions'=>array('readonly'=>($model->isNewRecord? TRUE : FALSE )) // HTML options
        ));             
        ?>
        <?php echo $form->error($model,'dateentry'); ?>
        </div><!-- row -->
        <div class="row">
        <?php echo $form->labelEx($model,'reporttaskdescription'); ?>
        <?php echo $form->textArea($model, 'reporttaskdescription',
                                array('cols'=>50,
                                        'rows'=>5)); ?>
        <?php echo $form->error($model,'reporttaskdescription'); ?>
        </div><!-- row -->
        <div class="row">
        <?php echo $form->labelEx($model,'completed'); ?>
        <?php echo $form->checkBox($model, 'completed'); ?>
        <?php echo $form->error($model,'completed'); ?>
        </div><!-- row -->

        		<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		 <?php echo CHtml::activeFileField($model,'filename');  ?>
		<?php echo $form->error($model,'filename'); ?>
		</div><!-- row -->


<div class="row buttons">
    <?php 
        echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
</div>
    <?php $this->endWidget(); ?>
</div><!-- form -->