<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
</head>
<body>
    Estimado Estudiante.<br />Su gestión ha cambiado de estado dentro del Flujo de Aprobación.</b> 
    <br>   
    <h3>Datos</h3>
    <table width="600" border="0">
        <tr>            
            <td width="10%"><strong># de Gestión:</strong></td>
            <td width="40%"><?php echo $myMail->id; ?></td>
        </tr>
        <tr>
            <td width="10%"><strong>Tipo:</strong></td>
            <td width="40%"><?php echo Yii::app()->user->GetState('TipoGestion'); ?></td>
        </tr>
        <tr>            
            <td width="10%"><strong>Estado:</strong></td>
            <td width="40%"><?php echo $myMail->status->statusdescription; ?></td>
        </tr>
        <tr>            
            <td width="10%"><strong>Fecha/hora:</strong></td>
            <td width="40%"><?php echo date("d-M-Y H:i"); ?></td>
        </tr>
           
    </table>
    <br />
    <?php if ($myMail->student->career->contactInfo && $myMail->student->career->contactInfo!='') { ?>
        Para cualquier duda o consulta al respecto, puede comunicarse a 
        <strong><?php echo $myMail->student->career->contactInfo; ?></strong> 
        <?php if ($myMail->student->career->email && $myMail->student->career->email!='') { ?>
            o al correo <strong><a href="mailto:<?php echo $myMail->student->career->email; ?>"><?php echo $myMail->student->career->email; ?></a></strong>
        <?php } ?>
        .
    <?php } 
        else {
    ?>
        Para cualquier duda o consulta al respecto, puede comunicarse al correo 
        <strong><a href="mailto:<?php echo $myMail->student->career->email; ?>"><?php echo $myMail->student->career->email; ?></a></strong>. 
    <?php } ?>
    
    <br />
    <br />
    <br />
    <h3><?php echo Yii::app()->params['emailFooter']; ?></h3>
</body>
</html>