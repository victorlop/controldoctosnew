<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
</head>
<body>
    Estimado Catedrático.<br />Un nuevo Documento le ha sido asignado para su Revisión.</b> 
    <br>   
    <h3>Datos</h3>
    <table width="600" border="0">
        <tr>            
            <td width="10%"><strong># de Gestión:</strong></td>
            <td width="40%"><?php echo $myMail->requestid; ?></td>
        </tr>
        <tr>
            <td width="10%"><strong>Tipo:</strong></td>
            <td width="40%"><?php echo Yii::app()->user->GetState('TipoGestion'); ?></td>
        </tr>
        <tr>            
            <td width="10%"><strong>Fecha/hora:</strong></td>
            <td width="40%"><?php echo date("d-M-Y H:i"); ?></td>
        </tr>
           
    </table>
    <br />
    <br />
    <br />
    <br />
    <h3><?php echo Yii::app()->params['emailFooter']; ?></h3>
</body>
</html>