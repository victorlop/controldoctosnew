<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
</head>
<body>
    Estimado Estudiante, ha sido registrado dentro del sistema de 
    <b>Control del Flujo de Documentos para Proyectos de Investigación</b>,
    con lo cual esta habilitado para el ingreso de Solicitudes de Aprobación de Tema, 
    trabajar su documento de Anteproyecto  y finalmente su documento de Proyecto de Investigación e informe final.
    <br>   
    <h3>Datos de registro</h3>
    <table width="600" border="0">
        <tr>
            <td width="20%"><strong>Carnet</strong></td>
            <td width="40%"><strong>Nombre</strong></td>
            <td width="40%"><strong>Carrera</strong></td>
        </tr>
        <tr>
            <td width="20%"><?php echo $myMail->studentbatchid; ?></td>
            <td width="40%"><?php echo $myMail->studentname; ?></td>
            <td width="40%"><?php echo $myMail->career->careerdescription; ?></td>
        </tr>
           
    </table>
    <br />
    <br />
    <br />
    <h3><?php echo Yii::app()->params['emailFooter']; ?></h3>    
</body>
</html>