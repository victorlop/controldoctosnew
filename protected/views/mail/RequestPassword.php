<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
</head>
<body>
    Estimado usuario. <br />Ha recibido este correo de acuerdo a la solicitud hecha 
    para la recuperación de su clave de acceso al sistema <b>Control del Flujo de Documentos para Proyectos de Investigación.</b> 
    <br>   
    <h3>Datos</h3>
    <table width="600" border="0">
        <tr>
            <td width="10%"><strong>Usuario:</strong></td>
            <td width="40%"><?php echo $myMail->username; ?></td>
        </tr>
        <tr>            
            <td width="10%"><strong>Clave:</strong></td>
            <td width="40%"><?php echo $myMail->passwordnew ?></td>
        </tr>
           
    </table>
    <br />
    Por su seguridad, es recomendable que una vez haya ingresado al sistema, <strong>cambie</strong> inmediatamente su clave de acceso.
    
    <br />
    <br />
    <br />
    <h3><?php echo Yii::app()->params['emailFooter']; ?></h3>
</body>
</html>