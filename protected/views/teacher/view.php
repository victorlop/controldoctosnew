<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<h1><?php echo Yii::t('app', 'Detalle')  ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'teachername',
            'email',
            array(
                'name'=>'canreview',
                'type'=>'raw',
                'value'=>$model->canreview=="1"?("Si"):("No"),
             ),
	),
)); ?>

<h2><?php /*echo GxHtml::encode($model->getRelationLabel('coursebyteachers'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->coursebyteachers as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('coursebyteacher/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('preprojectreviews'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->preprojectreviews as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('preprojectreview/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('projectreviews'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->projectreviews as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('projectreview/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?>