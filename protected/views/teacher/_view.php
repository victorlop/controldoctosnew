<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('teachername')); ?>:
	<?php echo GxHtml::encode($data->teachername); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('canreview')); ?>:
	<?php echo GxHtml::encode($data->canreview); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />

</div>