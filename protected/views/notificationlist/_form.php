<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'notificationlist-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'listdescription'); ?>
		<?php echo $form->textField($model, 'listdescription', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'listdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('flowdoctoDetails')); ?></label>
		<?php echo $form->checkBoxList($model, 'flowdoctoDetails', GxHtml::encodeEx(GxHtml::listDataEx(FlowdoctoDetail::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('notificationlistDetails')); ?></label>
		<?php echo $form->checkBoxList($model, 'notificationlistDetails', GxHtml::encodeEx(GxHtml::listDataEx(NotificationlistDetail::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->