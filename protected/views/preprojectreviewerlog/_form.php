<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preprojectreviewerlog-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
                <?php echo $form->hiddenField($model,'dateentry');?>
                <?php echo $form->hiddenField($model,'status');?>
        <?php echo $form->hiddenField($model,'preprojectreviewerid');?>
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                        array('rows'=>5, 'cols'=>60)); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		 <?php echo CHtml::activeFileField($model,'filename');  ?>
		<?php echo $form->error($model,'filename'); ?>
		</div><!-- row -->


                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->