<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('status-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Estados'); ?></h1>
<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'status-grid',
	'dataProvider' => $model->search(),
//	'filter' => $model,
	'columns' => array(
                array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
		'statusdescription',
		'amountreviews',
                /*array(
                       'name'=>'endstatus',
                       'header'=>'Es final',
                       'filter'=>array('1'=>'Si','0'=>'No'),
                       'value'=>'($data->endstatus=="1")?("Si"):("No")'
                    ),*/
		'maxdays',
		array(
                   'class'=>'EJuiDlgsColumn',
                       'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                       'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                       'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),

                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                
                    ),
                    'viewDialog'=>array(
                        'controllerRoute' => 'view', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Detalle - Estado',
                        'hideTitleBar' => true, 
                        'dialogWidth' => 600, //use the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar'
                    ),
                    'updateDialog'=>array(
                        'controllerRoute' => 'update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Modificar - Estado',
                        'hideTitleBar' => true,
                        'dialogWidth' => 700, //override the value from the dialog config
                        'dialogHeight' => 550,
                        'closeButtonText' => 'Cerrar'
                    ),            
               ),
	),
)); ?>
<div class="row buttons">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Estado',
        'dialogWidth' => 700,
        'dialogHeight' => 550,
        'hideTitleBar' => true,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'status-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey'),
    )
);
?>
</div>