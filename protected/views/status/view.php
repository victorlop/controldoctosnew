<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<h1><?php echo Yii::t('app', 'Detalle')  ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'statusdescription',
            'amountreviews',
            /*array(
                'name'=>'endstatus',
                'type'=>'raw',
                'value'=>$model->endstatus=="1"?("Si"):("No"),
             ),*/

            'maxdays',
            array(
                'name'=>'allowinactivation',
                'type'=>'raw',
                'value'=>$model->endstatus=="1"?("Si"):("No"),
             ),
	),
)); ?>

<h2><?php /*echo GxHtml::encode($model->getRelationLabel('flowdoctoDetails'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->flowdoctoDetails as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('flowdoctoDetail/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('preprojects'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->preprojects as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('preproject/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('preprojectreviews'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->preprojectreviews as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('preprojectreview/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('projects'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->projects as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('project/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('projectreviews'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->projectreviews as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('projectreview/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?><h2><?php /*echo GxHtml::encode($model->getRelationLabel('rolstatuses'));*/ ?></h2>
<?php
	/*echo GxHtml::openTag('ul');
	foreach($model->rolstatuses as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('rolstatus/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?>