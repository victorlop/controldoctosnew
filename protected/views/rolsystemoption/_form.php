<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rolsystemoption-form',
	'enableAjaxValidation' => TRUE,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>
	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>
		<div class="row">
                    <?php echo $form->labelEx($model,'rolid'); ?>
                    <?php echo $form->dropDownList($model, 'rolid', GxHtml::listDataEx(Rol::model()->findAllAttributes(null, true)),
                        array('disabled'=>TRUE,)); ?>
                    <?php echo $form->error($model,'rolid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php 
                    $criteria = new CDBCriteria();
                    $criteria->select=array('systemoptionid');
                    $criteria->condition= 'rolid=:rolid ';
                    $criteria->params=array(':rolid'=>$model->rolid);
                    
                    $opciones= Rolsystemoption::model()->findAll($criteria);
                    $opcInc=array();
                    foreach ($opciones as $opc) {
                        $opcInc[] = $opc->systemoptionid;
                    }
                
                    $criteria = new CDbCriteria;
                    $criteria->select = array('id,description');
                    $criteria->addNotInCondition('id',$opcInc,TRUE);
                    
                    echo $form->labelEx($model,'systemoptionid'); ?>
                    <?php echo $form->dropDownList($model, 'systemoptionid', GxHtml::listDataEx(Systemoption::model()->findAllAttributes(null, true,$criteria))); ?>
                    <?php echo $form->error($model,'systemoptionid'); ?>
		</div><!-- row -->


            <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app','Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div>