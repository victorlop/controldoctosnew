<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('rolid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->rol)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('systemoptionid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->systemoption)); ?>
	<br />

</div>