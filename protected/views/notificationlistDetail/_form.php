<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'notificationlist-detail-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'notificationemail'); ?>
		<?php echo $form->textField($model, 'notificationemail', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'notificationemail'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'notificationlistid'); ?>
		<?php echo $form->dropDownList($model, 'notificationlistid', GxHtml::listDataEx(Notificationlist::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'notificationlistid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->