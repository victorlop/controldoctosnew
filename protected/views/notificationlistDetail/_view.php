<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('notificationemail')); ?>:
	<?php echo GxHtml::encode($data->notificationemail); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notificationlistid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->notificationlist)); ?>
	<br />

</div>