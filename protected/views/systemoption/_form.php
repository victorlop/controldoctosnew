<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'systemoption-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model, 'description', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'keyvalue'); ?>
		<?php echo $form->textField($model, 'keyvalue', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'keyvalue'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'moduleid'); ?>
		<?php echo $form->dropDownList($model, 'moduleid', GxHtml::listDataEx(Module::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'moduleid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'path'); ?>
		<?php echo $form->textField($model, 'path', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'path'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('rolsystemoptions')); ?></label>
		<?php echo $form->checkBoxList($model, 'rolsystemoptions', GxHtml::encodeEx(GxHtml::listDataEx(Rolsystemoption::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->