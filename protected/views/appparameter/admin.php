<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('appparameter-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Parametros'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'appparameter-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
        'filterPosition'=>'none',
	'columns' => array(
		'id',
		'code',
		'parameterdescription',
		//'parametervalue',   
                array(
                    'name'=>'Valor',
                    'value'=> function($data,$row) {
                                if ($data->code == 'REQ' or $data->code == 'ANT' or $data->code == 'PRJ') {
                                    $valor = (int)$data->parametervalue;
                                    $Flow = Flowdocto::model()->findByPk($valor);

                                    if ($Flow)
                                        return $Flow->flowdescription;
                                    else
                                        return $data->parametervalue;
                                }
                                else if ($data->code == 'MAILSTU' or $data->code == 'MAILREV')
                                {
                                    $valor = (int)$data->parametervalue;
                                    if ($valor == 0)
                                        return 'No';
                                    else
                                        return 'Si';
                                }
                                else {
                                    return $data->parametervalue;
                                }
                            } ,
                ),
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{update}',
                    'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',                    
                    'updateDialog'=>array(
                        'controllerRoute' => 'update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Modificar - Parámetros',
                        //'hideTitleBar'=>true,
                        'dialogWidth' => 600, //override the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar'
                    ), 
               ),
	),
)); ?>