<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('coursename')); ?>:
	<?php echo GxHtml::encode($data->coursename); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coursecode')); ?>:
	<?php echo GxHtml::encode($data->coursecode); ?>
	<br />

</div>