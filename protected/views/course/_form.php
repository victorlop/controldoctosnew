<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'course-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'coursename'); ?>
		<?php echo $form->textField($model, 'coursename', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'coursename'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'coursecode'); ?>
		<?php echo $form->textField($model, 'coursecode', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'coursecode'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('coursebystudents')); ?></label>
		<?php echo $form->checkBoxList($model, 'coursebystudents', GxHtml::encodeEx(GxHtml::listDataEx(Coursebystudent::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('coursebyteachers')); ?></label>
		<?php echo $form->checkBoxList($model, 'coursebyteachers', GxHtml::encodeEx(GxHtml::listDataEx(Coursebyteacher::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->