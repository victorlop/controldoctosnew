<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coursebyteacher-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'courseid'); ?>
		<?php echo $form->textField($model, 'courseid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'courseid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'teacherid'); ?>
		<?php echo $form->textField($model, 'teacherid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'teacherid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->