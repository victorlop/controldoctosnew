<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rolstatus-form',
	'enableAjaxValidation' => TRUE,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>

		<div class="row">
		<?php echo $form->labelEx($model,'rolid'); ?>
		<?php echo $form->dropDownList($model, 'rolid', GxHtml::listDataEx(Rol::model()->findAllAttributes(null, true)),
                        array('disabled'=>TRUE,)); ?>
		<?php echo $form->error($model,'rolid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusid'); ?>
		<?php 
                
                $criteria = new CDBCriteria();
                    /*$criteria->with=  array('rolsystemoptions');
                    $criteria->together=TRUE;     
                    $criteria->select=array('systemoption.id, systemoption.description');
                          
                    $criteria->condition= 'rolid=:rolid ';
                    $criteria->params=array(':rolid'=>$model->rolid);*/
                    $criteria->select=array('statusid');
                    $criteria->condition= 'rolid=:rolid ';
                    $criteria->params=array(':rolid'=>$model->rolid);
                    
                 $opciones= Rolstatus::model()->findAll($criteria);
                 $opcInc=array();   
                 foreach ($opciones as $opc) {
                    $opcInc[] = $opc->statusid;
                 }

                 $criteria = new CDbCriteria;
                 $criteria->select = array('id,statusdescription');
                 $criteria->addNotInCondition('id',$opcInc,TRUE);
                
                echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true,$criteria))); ?>
		<?php echo $form->error($model,'statusid'); ?>
		</div><!-- row -->


                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->