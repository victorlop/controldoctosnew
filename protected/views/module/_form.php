<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'module-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'descripcionmodule'); ?>
		<?php echo $form->textField($model, 'descripcionmodule', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'descripcionmodule'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'controller'); ?>
		<?php echo $form->textField($model, 'controller', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'controller'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'defaultpage'); ?>
		<?php echo $form->textField($model, 'defaultpage', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'defaultpage'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('systemoptions')); ?></label>
		<?php echo $form->checkBoxList($model, 'systemoptions', GxHtml::encodeEx(GxHtml::listDataEx(Systemoption::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->