<h1><?php echo Yii::t('app', 'Documentos por Solicitud'); ?></h1>
        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                        array('label' => 'Estudiante', 
                            'value'=> $model->student->studentbatchid . ' - ' . $model->student->studentname 
                        ,),
                        array('label'=>'Comentarios',
                            'value'=> $model->comment,),
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByRequest($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            'documentname',
                            'documentdescription',
                            array(
                                     'class'=>'EJuiDlgsColumn',
                                     'template'=>'{update}{delete}',
                                     'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                                     'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',  
                                     //'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("document/delete", array("id"=>$data->primaryKey))',
                                                     'visible'=>  '!DocumentsFlowOperation::CanModifyRequest(Request::model()->findByPk($data->requestid))',
                                                 ),
                                                'viewS' => array(
                                                    'label'=> 'Ver detalle',
                                                     'url'=>'Yii::app()->createUrl("document/view", array("id"=>$data->primaryKey))',
                                                     'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                                                     'options' => array(
                                                        'ajax' => array(
                                                            'type' => 'POST',
                                                            'url' => "js:$(this).attr('href')",
                                                            'update' => '#detail-section',
                        ),
                    ),
                                                 ),
                                                
                                        ),
                                   'viewDialog'=>array(
                                        'controllerRoute' => 'document/view', //=default
                                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                                        'dialogTitle' => 'Detalle - Documento',
                                        //'hideTitleBar' => true, 
                                        'dialogWidth' => 600, //use the value from the dialog config
                                        'dialogHeight' => 400,
                                        'closeButtonText' => 'Cerrar'
                                    ),
                                    'updateDialog'=>array(
                                        'controllerRoute' => 'document/update', //=default
                                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                                        'dialogTitle' => 'Modificar - Documento',
                                        'hideTitleBar' => true,                         
                                        'dialogWidth' => 700, //override the value from the dialog config
                                        'dialogHeight' => 550,
                                        'closeButtonText' => 'Cerrar'
                                    ),  
                                ),
                            ),
                    ));
            
        ?>
<div class="row buttons">    
<?php 
    if (!DocumentsFlowOperation::CanModifyRequest($model) && DocumentsFlowOperation::RequestGetEndStatus($model->id) )
    {
        EQuickDlgs::iframeButton(
            array(
                'controllerRoute' => 'document/createrequest/'.$parentId,
                'dialogTitle' => 'Agregar ',
                'dialogWidth' => 700,
                'dialogHeight' => 550,
                'openButtonText' => 'Agregar',
                'closeButtonText' => 'Cerrar',
                'hideTitleBar' => TRUE,
                'closeOnAction' => true, //important to invoke the close action in the actionCreate
                'refreshGridId' => 'child-grid', //the grid with this id will be refreshed after closing
                'openButtonHtmlOptions' => array('class'=>'btn-grey','id'=>'btDis')
            )
        );
    }
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('request/admin'),'class'=>'btn-grey')); ?>
</div>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'dlg-detail',
        'options' => array(
            'title' => 'Dialog Box Title',
            'closeOnEscape' => true,
            'autoOpen' => false,
            'model' => false,
            'width' => 550,
            'height' => 450,
        ),
)) ?>
<div id="detail-section"></div>
<?php $this->endWidget() ?>
<!-- http://www.yiiframework.com/forum/index.php/topic/32985-extension-quickdlgs/ -->