<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'request-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
            
		<div class="row">
		<?php echo $form->labelEx($model,'studentid'); ?>
		<?php echo $form->dropDownList($model, 'studentid', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1), array('prompt' => Yii::t('app', 'Todos')))),array('style'=>'width: 30%')); ?>
		<?php echo $form->error($model,'studentid'); ?>
		</div><!-- row -->
                <?php
                if (!$model->isNewRecord)
                { ?>
                    <div class="row">
                    <?php echo $form->labelEx($model,'entrydate'); ?>
                    <?php echo $form->textField($model, 'entrydate',array('disabled'=>TRUE,'style'=>'width: 30%')); ?>
                    <?php echo $form->error($model,'entrydate'); ?>
                    </div><!-- row -->
                <?php 
                }
                else
                    echo $form->hiddenField($model,'entrydate');
                ?>
                    <div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
                <?php 
                    if ($model->isNewRecord)
                    {
                        /*echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All')));*/ 
                        echo $form->hiddenField($model,'statusid');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)')));
                        //echo $form->hiddenField($model,'flowdoctoid',array('value'=>$valpar));
                    }
                    else
                    {
                        $dis = DocumentsFlowOperation::CanModifyRequest($model);
                        
                        echo $form->hiddenField($model,'statusid');
                        //echo $form->hiddenField($model,'flowdoctoid');
                        echo $form->dropDownList($model, 'flowdoctoid', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', '(seleccionar)'),'disabled'=>$dis));
                    }
                ?>
                <?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model, 'comment',
                                            array('cols'=>50, 'rows'=>5)); ?>
		<?php echo $form->error($model,'comment'); ?>
		</div><!-- row -->

		<label><?php /*echo GxHtml::encode($model->getRelationLabel('requestdetails'));  */ ?></label>
		<?php /*echo $form->checkBoxList($model, 'requestdetails', GxHtml::encodeEx(GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)), false, true)); */?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('requestreviewlogs')); */ ?></label>
		<?php /*echo $form->checkBoxList($model, 'requestreviewlogs', GxHtml::encodeEx(GxHtml::listDataEx(Requestreviewlog::model()->findAllAttributes(null, true)), false, true));*/ ?>
                <div class="row buttons">
                    <?php echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar')); ?>
                </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
<?php 
/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'requestdetail-grid',
	'dataProvider' => $model->requestdetails,
	//'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'topicid',
				'value'=>'GxHtml::valueEx($data->topic)',
				'filter'=>GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true)),
				),
		'approved',
		array(
			'class' => 'CButtonColumn',
		),
	),
));*/