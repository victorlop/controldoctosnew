<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('request-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Solicitudes de Tema - Ingresos últimos 30 días'); ?></h1>
<?php /*
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));*/
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'request-grid',
	'dataProvider' => $model->lastentries(),
	//'filter' => $model,
	'columns' => array(
		//array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
				'name'=>'studentid',
				'value'=>'GxHtml::valueEx($data->student)',
                                'htmlOptions'=>array('width'=>'125'),
				'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
				),
                array('name'=>'entrydate','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		array('name'=>'comment', 'htmlOptions'=>array('width'=>'100')),
		array(
				'name'=>'statusid',
                                'filter'=>false,
                                'htmlOptions'=>array('width'=>'150'),
				'value'=>'GxHtml::valueEx($data->status)',
				),
		
               ),
	
)); ?>
<div class="row buttons">
<?php 
/*
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Solicitud',
        'dialogWidth' => 600,
        'dialogHeight' => 500,
        'hideTitleBar' => true,         
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'request-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey')
    )
);
*/
?>
</div>