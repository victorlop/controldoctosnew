<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Agregar'=> $this->renderPartial('_form', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>false,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%; height:100%;'
    ),
));
?>