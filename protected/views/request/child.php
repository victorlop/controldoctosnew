<h1><?php echo Yii::t('app', 'Temas por Solicitud'); ?></h1>
        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                        array('label' => 'Estudiante', 
                            'value'=> $model->student->studentbatchid . ' - ' . $model->student->studentname 
                        ,),
                        array('label'=>'Comentarios',
                            'value'=> $model->comment,),
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByParent($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            array(
				'name'=>'topicid',
				'value'=>'GxHtml::valueEx($data->topic)',
				'filter'=>GxHtml::listDataEx(Topic::model()->findAll()),
				),
                            array(
                                    'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                                
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("requestdetail/delete", array("id"=>$data->primaryKey))',
                                                     'visible'=>  '!DocumentsFlowOperation::CanModifyRequest($data->request)',
                                                 ),

                                    ),
                                ),
                            ),
                    ));
            
        ?>
<div class="row buttons">    
<?php 
    if (!DocumentsFlowOperation::CanModifyRequest($model) &&
        DocumentsFlowOperation::CanModifyRequestMaxTopic($model))
    {
        EQuickDlgs::iframeButton(
            array(
                'controllerRoute' => 'requestdetail/create/'.$parentId,
                'dialogTitle' => 'Agregar ',
                'dialogWidth' => 600,
                'dialogHeight' => 400,
                'openButtonText' => 'Agregar',
                'closeButtonText' => 'Cerrar',
                'hideTitleBar' => TRUE,
                'closeOnAction' => true, //important to invoke the close action in the actionCreate
                'refreshGridId' => 'child-grid', //the grid with this id will be refreshed after closing
                'openButtonHtmlOptions' => array('class'=>'btn-grey','id'=>'btDis')
            )
        );
    }
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('request/admin'),'class'=>'btn-grey')); ?>
</div>
<!-- http://www.yiiframework.com/forum/index.php/topic/32985-extension-quickdlgs/ -->