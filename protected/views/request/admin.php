<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('request-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Solicitudes de Tema'); ?></h1>

<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'request-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
				'name'=>'studentid',
				'value'=>'GxHtml::valueEx($data->student)',
                                'htmlOptions'=>array('width'=>'150'),
				'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
				),
                array('name'=>'entrydate','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		array('name'=>'comment', 'htmlOptions'=>array('width'=>'150')),
		array(
				'name'=>'statusid',
                                'filter'=>false,
                                'htmlOptions'=>array('width'=>'100'),
				'value'=>'GxHtml::valueEx($data->status)',
				),
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{detail}{docum}',
                    'viewButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_view.png',
                    'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_edit.png',
                    'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                    
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                    'visible' => 'DocumentsFlowOperation::RequestGetEndStatus($data->primaryKey)',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                   'visible'=>  '!DocumentsFlowOperation::CanModifyRequest($data)',
                                ),
                                'detail' => array(
                                    'label' => 'Temas',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    'url'=>'Yii::app()->createUrl("request/child", array("id"=>$data->primaryKey))',
                                ),
                                'docum' => array(
                                    'label' => 'Documentos',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/attach.png',
                                    'url'=>'Yii::app()->createUrl("request/documents", array("id"=>$data->primaryKey))',
                                ),/*
                                'status'=>array(
                                    'label' => 'Estados donde participa',
                                    'imageUrl'=>Yii::app()->baseUrl .'/images/rol_status.png',
                                    'url'=>'Yii::app()->createUrl("rol/status", array("id"=>$data->primaryKey))',
                                ),*/
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Solicitud',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 500,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Solicitud',
                       'hideTitleBar' => true,                        
                       'dialogWidth' => 600, //override the value from the dialog config
                       'dialogHeight' => 500,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>
<div class="row buttons">
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Solicitud',
        'dialogWidth' => 600,
        'dialogHeight' => 500,
        'hideTitleBar' => true,         
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'request-grid', //the grid with this id will be refreshed after closing
        'openButtonHtmlOptions' => array('class'=>'btn-grey')
    )
);

?>
</div>