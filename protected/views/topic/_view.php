<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('topicdescription')); ?>:
	<?php echo GxHtml::encode($data->topicdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('topicused')); ?>:
	<?php echo GxHtml::encode($data->topicused); ?>
	<br />

</div>