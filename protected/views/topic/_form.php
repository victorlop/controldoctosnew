<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'topic-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'topicdescription'); ?>
		<?php echo $form->textField($model, 'topicdescription', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'topicdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'topicused'); ?>
		<?php echo $form->textField($model, 'topicused'); ?>
		<?php echo $form->error($model,'topicused'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('requestdetails')); ?></label>
		<?php echo $form->checkBoxList($model, 'requestdetails', GxHtml::encodeEx(GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->