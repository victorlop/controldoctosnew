<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comments')); ?>:
	<?php echo GxHtml::encode($data->comments); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('projectreviewid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->projectreview)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('filename')); ?>:
	<?php echo GxHtml::encode($data->filename); ?>
	<br />

</div>