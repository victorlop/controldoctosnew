<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('projectreviewlog-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Revisiones Proyecto'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model_parent,
                'attributes' => array(
                        array('label' => 'Anteproyecto', 
                            'value'=> $model_parent->project->projectdescription
                        ,),
                        array('label' => 'Estudiante', 
                            'value'=> $model_parent->project->student->studentbatchid . ' - ' . $model_parent->project->student->studentname 
                        ,),
                        array('label' => 'Fecha Asignacion', 
                            'value'=> $model_parent->dateentry
                        ,),
                        array('label'=>'Comentarios',
                            'value'=> $model_parent->project->comments,),
                ),
        )); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'projectreviewlog-grid',
	'dataProvider' => $model->searchByParent($parentId),
	//'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array('name'=>'dateentry','htmlOptions'=>array('width'=>'120'),),
		array('name'=>'comments','type'=>array('type'=>'shortText','length'=>80,)),
                 array(
                    'name'=>'name',
                    'htmlOptions'=>array('width'=>1,'style' => 'text-align: right;'),
                    'type'=>'raw',
                    'header'=>'',
                    'value'=>function($data,$row) {
    
                                                $docto = Document::model()->find('projectreviewlogid=:id',array(':id'=>$data->primaryKey));
                                                if ($docto)
                                                {
                                                    $review= Projectreviewlog::model()->findByPk($data->primaryKey);
                                                    $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');
                                                    $uploadPath = Yii::app()->params['uploadUrl']; 
                                                    $uploadPath = Yii::app()->baseUrl.$uploadPath .'/project/'.$review->projectreview->projectid.'/'.$review->projectreview->statusid.'/'.$data->primaryKey;
                                                    $uploadPath = $uploadPath.'/['.$data->primaryKey.']'.$docto->filename;

                                                    //return Yii::app()->createUrl($uploadPath);
                                                    return CHtml::link($image,$uploadPath , array('target'=>'_blank','title'=>$docto->documentdescription)); 
                                                }
                                                return null;
                                            },
                
            ),
		/*'status',
		array(
				'name'=>'preprojectreviewerid',
				'value'=>'GxHtml::valueEx($data->preprojectreviewer)',
				'filter'=>GxHtml::listDataEx(Preprojectreviewer::model()->findAllAttributes(null, true)),
				),*/
		 array(
                        'class'=>'EJuiDlgsColumn',
                         'template'=>'{delete}',
                         'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/icon_delete.png',                                
                         'htmlOptions'=>array('width'=>'80','style' => 'text-align: left;'),
                            'buttons'=>array(
                                     'delete' => array(
                                        'label'=> 'Eliminar',
                                         'url'=>'Yii::app()->createUrl("delete", array("id"=>$data->primaryKey))',
                                         'visible'=>'$data->projectreview->ended=="1"? FALSE: TRUE',
                                     ),
                                    'doctos' => array(
                                        'label'=> 'Documentos',
                                        'imageUrl'=>Yii::app()->theme->baseUrl .'/images/attach.png',
                                        /*'visible'=>'function($data,$row) {
                                                        $docto = Document::model()->count("preprojectreviewerlogid=:id",array(":id"=>$data->primaryKey));
                                                        return $docto > 0;
                                                    }',*/
                                         'url'=>function($data,$row) {
    
                                                $docto = Document::model()->find('projectreviewerlogid=:id',array(':id'=>$data->primaryKey));
                                                $review= Preprojectreviewerlog::model()->findByPk($data->primaryKey);
                                                
                                                
                                                $uploadPath = Yii::app()->baseUrl.'/'. Yii::app()->params['uploadPath']; 
                                                $uploadPath = $uploadPath .'/preproject/'.$review->preprojectreviewer->preprojectid.'/'.$review->preprojectreviewer->statusid.'/'.$data->primaryKey;
                                                $uploadPath = $uploadPath.'/['.$data->primaryKey.']'.$docto->filename;
                                                
                                                //return Yii::app()->createUrl($uploadPath);
                                                return CHtml::link($uploadPath , array('target'=>'_blank')); 
                                            } ,
//'Yii::app()->createUrl("update", array("id"=>$data->primaryKey))',
                                        
                                    ),

                            ),
                    ),
                ),
)); ?>
<div class="row buttons">
<?php 
if ($model_parent->ended!=1)
{
    EQuickDlgs::iframeButton(
        array(
            'controllerRoute' => 'projectreviewlog/create/'.$parentId,
            'dialogTitle' => 'Agregar',
            'dialogWidth' => 600,
            'dialogHeight' => 400,
            'hideTitleBar' => true,
            'openButtonText' => 'Agregar',
            'closeButtonText' => 'Cerrar',
            'closeOnAction' => true, //important to invoke the close action in the actionCreate
            'refreshGridId' => 'projectreviewlog-grid', //the grid with this id will be refreshed after closing
            'openButtonHtmlOptions' => array('class'=>'btn-grey')
        )
    );
}
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('pendingsReviews/admin'),'class'=>'btn-grey')); ?>
</div>