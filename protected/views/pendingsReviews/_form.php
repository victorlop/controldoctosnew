<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'pendings-reviews-form',
	'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
        
            if ($model->id < 1000000)
                $model->id = $model->id * ($model->TipoId * 1000000);
?>

                <?php //echo $form->errorSummary($model); ?>

                        <?php echo $form->hiddenField($model, 'TipoId'); ?>
                <?php echo $form->hiddenField($model, 'Tipo'); ?>
                <?php echo $form->hiddenField($model, 'studentid'); ?>
                <?php echo $form->hiddenField($model, 'flowdoctoid'); ?>
                <?php echo $form->hiddenField($model, 'id'); ?>
                <?php echo $form->hiddenField($model, 'rolid'); ?>
                <?php echo $form->hiddenField($model, 'dateend'); ?>
                <?php echo $form->hiddenField($model, 'reviewid'); ?>
                <?php echo $form->hiddenField($model, 'statusid'); ?>  
                <?php echo $form->hiddenField($model, 'entrydate'); ?>      
                <?php echo $form->hiddenField($model, 'comments'); ?>      
    
    
    
                
                    <input type="hidden" id="id1" maxlength="21" disabled="true" 
                           value="<?php 
                                        echo $model->id / ($model->TipoId * 1000000);
                                    ?>" >
                <?php //echo $form->textField($model, 'id', array('maxlength' => 21,'disabled'=>TRUE)); ?>


		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry',array('size'=>30,'disabled'=>TRUE)); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->                
                <div class="row">
                <?php echo $form->labelEx($model,'studentname'); ?>
                <?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50,'disabled'=>TRUE)); echo $form->textField($model, 'studentname', array('maxlength' => 256,'size'=>50,'disabled'=>TRUE));?>
                <?php //echo $form->error($model,'studentbatchid'); ?>
                </div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024,'size'=>80,'disabled'=>TRUE)); ?>
		<?php echo $form->error($model,'preprojectdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'approved'); ?>
                <?php echo $form->dropDownList($model, 'approved',array('No','Si') ); ?>
		<?php echo $form->error($model,'approved'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ended'); ?>
                <?php echo $form->dropDownList($model, 'ended',array('No','Si') ); ?>
		<?php echo $form->error($model,'ended'); ?>
		</div><!-- row -->
                <?php
                $docId = $model->id / ($model->TipoId * 1000000);
                $criteria = new CDbCriteria(array('order'=>'id DESC', 'limit'=>1));
                $criteria->condition='preprojectid=:preprojectid and statusid=:statusid';
                $criteria->params = array(':preprojectid'=>$docId,':statusid'=>$model->statusid);
                
                $docto = Document::model()->find($criteria);

                if ($docto)
                {
                ?>
                    <div class="row">
                    <?php echo CHtml::label('Ver','im1' ) ?>
                    <?php 
                        $uploadPath = Yii::app()->params['uploadUrl'];
                        $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');

                        $newPath =Yii::app()->baseUrl.$uploadPath. '/preproject/'.$docId.'/'.$model->statusid.'/['.$docId.']'.$docto->filename;
                        echo CHtml::link($image,$newPath , array('target'=>'_blank','id=>im1')); 
                    ?>
                    </div>
                <?php 
                }
                ?>
            <div class="row buttons">
            <?php
                echo GxHtml::submitButton(Yii::t('app', 'Grabar'));
            $this->endWidget();
            ?>
        </div>
           </div><!-- form -->