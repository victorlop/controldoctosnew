<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'Tipo'); ?>
		<?php echo $form->dropDownList($model, 'Tipo', array('Solicitud'=>'Solicitud','Ante-Proyecto'=>'Ante-Proyecto','Proyecto'=>'Proyecto'),array('prompt'=>'Todos')); ?>
	</div>

        <div class="row">
		<?php echo $form->label($model, 'studentid'); ?>
		<?php 
                    $students = Student::model()->findAll('status=:status',array(':status'=>1));
                    $studentA=array();
                    foreach ($students as $student)
                        $studentA[$student->id]=$student->studentbatchid . ' - ' . $student->studentname;

                    $this->widget('ext.widgets.select2.XSelect2', array(
                            'model'=>$model,
                            'attribute'=>'studentid',
                            'data'=>$studentA,
                            'htmlOptions'=>array('style'=>'width: 50%','prompt'=>'Todos'),
                            )                        
                    )
                                
                    //echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024)); ?>
	</div>



	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Filtrar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
