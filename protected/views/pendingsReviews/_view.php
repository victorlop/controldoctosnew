<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('Tipo')); ?>:
	<?php echo GxHtml::encode($data->Tipo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentid')); ?>:
	<?php echo GxHtml::encode($data->studentid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentname')); ?>:
	<?php echo GxHtml::encode($data->studentname); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentbatchid')); ?>:
	<?php echo GxHtml::encode($data->studentbatchid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('entrydate')); ?>:
	<?php echo GxHtml::encode($data->entrydate); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusid')); ?>:
	<?php echo GxHtml::encode($data->statusid); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('preprojectdescription')); ?>:
	<?php echo GxHtml::encode($data->preprojectdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('flowdoctoid')); ?>:
	<?php echo GxHtml::encode($data->flowdoctoid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rolid')); ?>:
	<?php echo GxHtml::encode($data->rolid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateend')); ?>:
	<?php echo GxHtml::encode($data->dateend); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('approved')); ?>:
	<?php echo GxHtml::encode($data->approved); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comments')); ?>:
	<?php echo GxHtml::encode($data->comments); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('reviewid')); ?>:
	<?php echo GxHtml::encode($data->reviewid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ended')); ?>:
	<?php echo GxHtml::encode($data->ended); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('TipoId')); ?>:
	<?php echo GxHtml::encode($data->TipoId); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('filename')); ?>:
	<?php echo GxHtml::encode($data->filename); ?>
	<br />
	*/ ?>

</div>