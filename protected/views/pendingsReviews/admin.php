<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('pendings-reviews-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Revisiones Pendientes'); ?></h1>
<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Filtros'=> $this->renderPartial('_search', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>true,
        'active'=>1,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));


?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'pendings-reviews-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		'id1' => array(
                    'name'=>'id',
                    'value'=> function($data)   {
                        
                        return ($data->id - $data->TipoId) / ($data->TipoId * 1000000);
                    }
                ),  
		'Tipo',
                array('name'=>'preprojectdescription','type'=>array('type'=>'shortText','length'=>50,'width'=>40,)),                        
		//'studentid',
		'studentname',
		'studentbatchid',
		'entrydate',
		array(
                    'name'=>'name',
                    'htmlOptions'=>array('width'=>1,'style' => 'text-align: right;'),
                    'type'=>'raw',
                    'header'=>'',
                    'value'=>function($data,$row) {
                                        if ($data->TipoId==3)
                                        {
                                                $id = $data->primaryKey - $data->TipoId;
                                                $id = $id / ($data->TipoId * 1000000);
                                                $review = Projectreview::model()->findByPk($id);
                                                
                                                //$docto = Document::model()->find('projectid=:id and statusid=:statusid',array(':id'=>$review->projectid,':statusid'=>$data->statusid));
                                                $criteria = new CDbCriteria(array('order'=>'id DESC', 'limit'=>1));
                                                $criteria->condition='projectid=:id and statusid=:statusid';
                                                $criteria->params = array(':id'=>$review->projectid,':statusid'=>$data->statusid);
                                                $docto = Document::model()->find($criteria);
                                                
                                                if ($docto)
                                                {                                            
                                                    $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');
                                                    $uploadPath = Yii::app()->params['uploadUrl']; 
                                                    $uploadPath = Yii::app()->baseUrl.$uploadPath .'/project/'.$review->projectid.'/'.$review->statusid;
                                                    $uploadPath = $uploadPath.'/['.$review->projectid.']'.$docto->filename;

                                                    //return Yii::app()->createUrl($uploadPath);
                                                    return CHtml::link($image,$uploadPath , array('target'=>'_blank','title'=>$docto->documentdescription)); 
                                                }
                                            }
                                            return null;
                                       },
                
            ),
		//'statusid',

		/*'flowdoctoid',
		'rolid',
		'dateentry',
		'dateend',
		'approved',
		'comments',
		'reviewid',
		'ended',
		'TipoId',
		'filename',
		*/
		array(
			'class' => 'EJuiDlgsColumn',
                        'template'=>'{update}{review}',
                        'updateButtonImageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                        'buttons'=>array(
                            'update' => array(
                                    'label' => 'Procesar',
                                    'visible'=> '!$data->ended',
                                    //'imageUrl'=>Yii::app()->theme->baseUrl .'/images/list.png',
                                    //'url'=>'Yii::app()->createUrl("update", array("id"=>$data->primaryKey))',        
                            ),
                            'review' => array(
                                    'label' => 'Anotaciones',
                                    'imageUrl'=>Yii::app()->theme->baseUrl .'/images/icon-chat2.png',
                                    'url'=>'Yii::app()->createUrl(($data->TipoId==2? "preprojectreviewerlog" :"projectreviewlog")."/admin", array("id"=>(($data->reviewid-2)/2000000)+((int)Yii::app()->user->GetState("id")*2000000)))',        
                                    
                            ),
                        ),
                    
                    'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->reviewid'), //=default
                       'dialogTitle' => 'Modificar - Flujo de Trabajo',
                        'hideTitleBar' => TRUE,                        
                       'dialogWidth' => 800, //override the value from the dialog config
                       'dialogHeight' => 500,
                       'closeButtonText' => 'Cerrar'
                   ), 
                ),    
	),
)); ?>
