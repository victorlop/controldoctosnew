<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('studentname')); ?>:
	<?php echo GxHtml::encode($data->studentname); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentbatchid')); ?>:
	<?php echo GxHtml::encode($data->studentbatchid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('careerid')); ?>:
	<?php echo GxHtml::encode($data->careerid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
</div>