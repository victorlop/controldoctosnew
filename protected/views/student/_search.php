<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<p style="font-size: smaller">Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.</p>
	<div class="row">
		<?php echo $form->label($model, 'studentbatchid'); ?>
		<?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50,'size'=>30)); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model, 'studentname'); ?>
		<?php echo $form->textField($model, 'studentname', array('maxlength' => 256,'size'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'careerid'); ?>
                <?php echo $form->dropDownList($model, 'careerid', GxHtml::listDataEx(Career::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'Todos'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
