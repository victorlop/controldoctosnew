<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'documentname'); ?>
		<?php echo $form->textArea($model, 'documentname'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'coursebyteacherid'); ?>
		<?php echo $form->textField($model, 'coursebyteacherid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'preprojectid'); ?>
		<?php echo $form->textField($model, 'preprojectid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'projectid'); ?>
		<?php echo $form->textField($model, 'projectid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'reviewlogid'); ?>
		<?php echo $form->textField($model, 'reviewlogid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'requestid'); ?>
		<?php echo $form->textField($model, 'requestid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'projectdetailid'); ?>
		<?php echo $form->textField($model, 'projectdetailid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'support'); ?>
		<?php echo $form->textField($model, 'support'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'documentdescription'); ?>
		<?php echo $form->textArea($model, 'documentdescription'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'filename'); ?>
		<?php echo $form->textArea($model, 'filename'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
