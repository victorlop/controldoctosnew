<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
        'documentname',
    )
)); ?>

<?php
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Actualizar'=> $this->renderPartial('_viewpart', array('model' => $model,),true)),
    'options'=>array(
        'collapsible'=>FALSE,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>