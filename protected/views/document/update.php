<?php
if ($model->requestid)
{
    $this->widget('zii.widgets.jui.CJuiAccordion', array(
        'panels'=>array(
            'Actualizar'=> $this->renderPartial('_formrequest', array('model' => $model,),true)),
        'options'=>array(
            'collapsible'=>FALSE,
            'active'=>0,
        ),
        'htmlOptions'=>array(
            'style'=>'width:100%;'
        ),
    ));
}
else if ($model->preprojectid)
{
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Actualizar'=> $this->renderPartial('_formpreproject', array('model' => $model,),true)),
    'options'=>array(
        'collapsible'=>FALSE,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));
}
?>