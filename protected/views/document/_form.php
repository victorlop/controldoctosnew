<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'document-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
                <?php echo $form->hiddenField($model,'coursebyteacherid'); ?>
                <?php echo $form->hiddenField($model,'preprojectid'); ?>
                <?php echo $form->hiddenField($model,'projectid'); ?>
                <?php echo $form->hiddenField($model,'reviewlogid'); ?>
                <?php echo $form->hiddenField($model,'requestid'); ?>
                <?php echo $form->hiddenField($model,'projectdetailid'); ?>
                <?php echo $form->hiddenField($model,'documentname',array('value'=>'xx')); ?>
		<?php echo $form->hiddenField($model,'support'); ?>
                <?php //echo $form->hiddenField($model,'filename'); ?>


    <div class="row">
	<?php echo $form->labelEx($model,'documentdescription'); ?>
	<?php echo $form->textArea($model, 'documentdescription'); ?>
	<?php echo $form->error($model,'documentdescription'); ?>
    </div><!-- row -->        
    <div class="row">
	<?php echo $form->labelEx($model,'filename'); ?>
	<?php echo CHtml::activeFileField($model,'filename');  ?>
	<?php echo $form->error($model,'filename'); ?>
    </div><!-- row -->       
<?php
echo GxHtml::submitButton(Yii::t('app', 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->