<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'document-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>
                <?php echo $form->hiddenField($model,'coursebyteacherid'); ?>
                <?php echo $form->hiddenField($model,'preprojectid'); ?>
                <?php echo $form->hiddenField($model,'projectid'); ?>
                <?php echo $form->hiddenField($model,'reviewlogid'); ?>
                <?php echo $form->hiddenField($model,'requestid'); ?>
                <?php echo $form->hiddenField($model,'projectdetailid'); ?>
                <?php //echo $form->hiddenField($model,'documentname',array('value'=>'xx')); ?>
		<?php echo $form->hiddenField($model,'support'); ?>
                <?php //echo $form->hiddenField($model,'filename'); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'documentname'); ?>
            <?php echo $form->textField($model, 'documentname',array('maxlength' => 50,'size'=>50, 'disabled'=> ($model->requestid? !DocumentsFlowOperation::RequestGetEndStatus($model->requestid) : FALSE))); ?>
            <?php echo $form->error($model,'documentname'); ?>
        </div><!-- row -->

        <div class="row">
            <?php echo $form->labelEx($model,'documentdescription'); ?>
            <?php echo $form->textArea($model, 'documentdescription',
                                            array('cols'=>50, 'rows'=>5,
                                                'disabled'=> ($model->requestid? !DocumentsFlowOperation::RequestGetEndStatus($model->requestid) : FALSE))); ?>
            <?php echo $form->error($model,'documentdescription'); ?>
        </div><!-- row -->    
        <?php 
            if ($model->isNewRecord)
            { ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'filename'); ?>
                    <?php echo CHtml::activeFileField($model,'filename');  ?>
                    <?php echo $form->error($model,'filename'); ?>
                </div><!-- row -->       
                <?php 
                    if(Yii::app()->user->hasFlash('error')): ?>
 
                        <div class="flash-error">
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                        </div>
 
                    <?php endif; 
            } 
            else
            { ?>
            <div class="row">
                <?php echo CHtml::label('Ver','im1' ) ?>
            <?php
                $uploadPath = Yii::app()->params['uploadUrl'];
                $image = CHtml::image(Yii::app()->theme->baseUrl .'/images/glass.png');
                $newPath =Yii::app()->baseUrl.$uploadPath. '/request/'.$model->requestid.'/['.$model->id.']'.$model->filename;
                
                echo CHtml::link($image,$newPath , array('target'=>'_blank','id=>im1')); 
            }
            ?>
            </div>
            <?php    if (($model->requestid? DocumentsFlowOperation::RequestGetEndStatus($model->requestid) : TRUE)) { ?>
                <div class="row buttons">
                    <?php
                    echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
                    ?>
                </div>
            <?php } ?>
        <p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<?php            
$this->endWidget();
?>

</div><!-- form -->
