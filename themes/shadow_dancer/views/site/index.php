<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Bienvenido al sistema de <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

    <li >
        <h5>PHP - Yii Framework </h5>
        <p>PHP provee una poderosa manera de construir sitios Web dinamicos que
        permiten una separacion clara y da control total sobre los marcadores
        para un desarrollo ágil. Yii Framework incluye muchas caracteristicas que permiten de una manera 
        rápida y amigable crear sofisticadas aplicaciones usando los últimos estandares de la industria.
        <a href="http://www.yiiframework.com" target="_blank">Leer mas…</a></p>
    </li>
    
    <li >
        <h5>Metodología "Proceso Unificado de Rational" (RUP, Rational Unified Process)</h5>
        <p>RUP, es un proceso de desarrollo de software desarrollado por la empresa Rational Software, 
        actualmente propiedad de IBM. Junto con el Lenguaje Unificado de Modelado UML, constituye 
        la metodología estándar más utilizada para el análisis, diseño, implementación y documentación 
        de sistemas orientados a objetos.
        <a href="http://www-01.ibm.com/software/rational/rup/" target="_blank">Leer mas…</a></p>
    </li>
    
    <li >
        <h5>jQuery</h5>
        <p>jQuery es una biblioteca de JavaScript, que permite simplificar la manera de interactuar con los documentos HTML, 
        manipular el árbol DOM, manejar eventos, agregar interacción con la técnica AJAX a páginas web, etc.
        jQuery es software libre y de código abierto, posee un doble licenciamiento bajo la Licencia MIT 
        y la Licencia Pública General de GNU v2.
        <a href="http://jQuery.com" target="_blank">Leer mas…</a></p>
    </li>
