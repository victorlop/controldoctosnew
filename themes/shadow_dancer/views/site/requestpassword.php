
<h1>Recuperación de Clave</h1>

<?php if(Yii::app()->user->hasFlash('requestsucess')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('requestsucess'); ?>
</div>

<?php elseif (Yii::app()->user->hasFlash('requesterror')): ?>
    <div class="flash-error">
	<?php echo Yii::app()->user->getFlash('requesterror'); ?>
    </div>
    <div class="row">
                <?php echo CHtml::link('Intentar nuevamente',Yii::app()->createUrl("site/requestpassword")); ?>
    </div>
<?php else: ?>


<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'RequestPassword-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<?php /*if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha',array('buttonOptions' => array('style' => 'display:block'))); ?>
        <br />
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
        <div class="hint">Ingrese las letras que aparecen en la imágen.</div>
	<?php endif;*/ ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Enviar',array('class'=>'button grey')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>

<?php
/* simulate a click on "refresh captcha" for GET requests */
if (!Yii::app()->request->isPostRequest)
    Yii::app()->clientScript->registerScript(
        'initCaptcha',
        '$(".captcha a").trigger("click");',
        CClientScript::POS_READY
    );
?>