<?php

?>
<h1>Tablero de control</h1>


<div class="container showgrid">
<div class="span-12">
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'title'=>'<span class="icon icon-folder_table">Solicitudes</span>',
));
?>
    <p>Ingresadas último mes: 
        <?php if (DashBoard::RequestLastMonth()>0) { ?>
            <a href="<?php echo Yii::app()->createUrl("request/lastentries") ?>">
            <?php echo DashBoard::RequestLastMonth(); ?></a>
        <?php } else echo 0 ?></p>
    <p></p>
    
<?php $this->endWidget();?>
</div>
<div class="span-11 last">
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'title'=>'<span class="icon icon-layout_sidebar">Anteproyectos</span>',
));
?>
<p>Ingresados último mes:
<?php if (DashBoard::PreProjectLastMonth()>0) { ?>
            <a href="<?php echo Yii::app()->createUrl("preproject/lastentries") ?>">
            <?php echo DashBoard::PreProjectLastMonth(); ?></a>
        <?php } else echo 0 ?></p>
    <p></p>
<?php $this->endWidget();?>
</div>
</div>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'title'=>'<span class="icon icon-medal_gold_3">Proyectos</span>',
));
?>
<p>Ingresados último mes: 
    <?php if (DashBoard::ProjectLastMonth()>0) { ?>
            <a href="<?php echo Yii::app()->createUrl("project/lastentries") ?>">
            <?php echo DashBoard::ProjectLastMonth(); ?></a>
        <?php } else echo 0 ?></p>
    <p></p>
    <p>Sin reporte de Avance en los últimos <?php echo AppParameterOperation::EnableValue('CHPDUE'); ?> días: 
    <?php if (DashBoard::ProjectLastUpdated()>0) { ?>
            <a href="<?php echo Yii::app()->createUrl("project/lastupdated") ?>">
            <?php echo DashBoard::ProjectLastUpdated(); ?></a>
        <?php } else echo 0 ?></p>
    <p></p>
<?php $this->endWidget();?>